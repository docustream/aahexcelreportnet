﻿using System;
using Excel = Microsoft.Office.Interop.Excel;

namespace dnUtil
{
    class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // v1.0.5 -- reorganized Program.cs file to be the entry point for program.
            System.Windows.Forms.Application.Run(new Form1());
        }
    }
}
