using System;
using System.Collections;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data;

namespace dnUtil
{
	/// <summary>
	/// Summary description for clsDocuProgram.
	/// </summary>
	public class clsDocuProgram
	{
		public static Form1 frmMain;
		public static string strLogFile;
		public static string strReport = "";
		public static bool boolStop;
		public static bool boolRunning;

		public clsDocuProgram()
		{
		}

		public bool Init(String strProgName, Form1 formMain)
		{
			frmMain = formMain;
			strLogFile = UtilFile.DirWithSlash(UtilFile.AppPath()) + strProgName + ".log";
			
			boolStop = false;
			boolRunning = false;
			return true;
		}

		public bool Start()
		{
			if(boolRunning)
			{
				return false;
			}

			string strStatus = "";
			strReport = UtilFile.DirWithSlash(frmMain.strReportFolder) + frmMain.strReportName;
			boolRunning = true;
			GUICmdState("START", "DISABLED");
			boolStop = false;

			GUIText("STATUS", "Running...");
			DoEvents();

			// DataTable will hold data for report
			DataTable dTblData = new DataTable();
			dTblData.Columns.Add("BatchName");
			dTblData.Columns.Add("ImageCount");
			dTblData.Columns.Add("ClaimCount");
			dTblData.Columns.Add("AttachmentCount");
			dTblData.Columns.Add("RejectCount");
			dTblData.Columns.Add("MergedImageCount");
			dTblData.Columns.Add("BatchDate");
			dTblData.Columns.Add("ReceivedDate");

			string strBatchName = "";
			int intImageCount = 0;
			int intClaimCount = 0;
			int intAttachmentCount = 0;
			int intRejectCount = 0;
			int intMergedImageCount = 0;
			string strBatchDate = "";
			string strReceivedDate = "";

			// populate files and subdirectories from specified directory
			clsDirectory cDir = new clsDirectory(frmMain.strZipFolder);
			cDir.Populate(true);

			GUIProgBar("MIN", 0);
			GUIProgBar("MAX", cDir.arrFile.Count);
			int i = 0;
			foreach(clsFile cFile in cDir.arrFile)
			{
				GUIProgBar("VALUE", i++);
				Application.DoEvents();

				// do for each zip file
				if(cFile.strFile.ToUpper().EndsWith(".ZIP"))
				{
					strBatchName = UtilFile.FileFromDir(cFile.strFile);
					string strDate = cDir.strDirectory.Substring(cDir.strDirectory.LastIndexOf("\\") + 1);
					strReceivedDate = UtilDate.DateGetNewFormat(strDate, "YYMMDD", "M/D/CCYY");

					if(strReceivedDate == "01/01/1900")
						strReceivedDate = strDate;

					// subfolder with images is the same name minus .zip
					string strSubFolder = cFile.strFile.Substring(0, cFile.strFile.LastIndexOf("."));
					int intCount = 0;
					bool boolZipFolderFound = false;

					foreach(clsDirectory cSubDir in cDir.arrDirectory)
					{
						if(cSubDir.strDirectory.ToUpper() == strSubFolder.ToUpper())
						{
							// count the number of images/claims
							foreach(clsFile cSubFile in cSubDir.arrFile)
							{
								if(cSubFile.strFile.ToUpper().EndsWith(".TIF"))
								{
									intCount++;
								}
							}

							intImageCount = intCount;
							intClaimCount = intCount;
							
							// get batch date from folder name
							strBatchDate = cSubDir.strDirectory.Substring(cSubDir.strDirectory.LastIndexOf("_") + 1);
							//strBatchDate = UtilDate.DateGetNewFormat(strBatchDate, "MMDDYY", "M/D/CCYY");
							strBatchDate = UtilDate.DateGetNewFormat(strBatchDate);

							boolZipFolderFound = true;
							break;
						}
					}

					// if a subfolder of the zip file was not found, continue
					if(!boolZipFolderFound)
					{
						MessageBox.Show("The zip file " + cFile.strFile + " must be unzipped into a folder of the same name",
							"", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						continue;
					}

					DataRow drNew = dTblData.NewRow();
					drNew["BatchName"] = strBatchName;
					drNew["ImageCount"] = intImageCount;
					drNew["ClaimCount"] = intClaimCount;
					drNew["AttachmentCount"] = intAttachmentCount;
					drNew["RejectCount"] = intRejectCount;
					drNew["MergedImageCount"] = intMergedImageCount;
					drNew["BatchDate"] = strBatchDate;
					drNew["ReceivedDate"] = strReceivedDate;

					dTblData.Rows.Add(drNew);
				}
			}

			// now we have a datatable filled with all the info we need for our report
			AAHReport rpt = new AAHReport();
			rpt.LogMessage = new LogFunction(Log);
			rpt.GUIMessage = new GUITextFunction(GUIText);
			rpt.GUIProgBarMessage = new GUIProgBarFunction(GUIProgBar);

			strStatus = rpt.GenerateReportExcel(strReport, dTblData);
			
			if (strStatus == "FINISHED")
			{
				GUIProgBar("VALUE", GUIProgBar("MIN", 0, true));
				GUIText("STATUS", "Done");
				GUICmdState("START", "ENABLED");
			}
			else if (strStatus == "STOPPED")
			{
				GUIText("STATUS", "Stopped");
				GUICmdState("START", "ENABLED");
				GUIProgBar("VALUE", GUIProgBar("MIN", 0, true));
			}
			else if(strStatus == "ERROR")
			{
				GUIText("STATUS", "Error -- Please check log.");
			}
			else
			{
				//we should never encounter this
				MessageBox.Show("FATAL ERROR!");
			}

			boolRunning = false;
			return true;
		}

		public void StopProgram()
		{
			boolStop = true;
			if(!boolRunning)
			{
				//If(the program already is running); this will be
				// taken care of after the batches/workflows are closed, etc
				GUIText("STATUS", "Stopped");
				GUICmdState("START", "ENABLED");
			}

			if(GUIProgBar("MIN", 0, true) != GUIProgBar("MAX", 0, true))
			{
				GUIProgBar("VALUE", GUIProgBar("MIN", 0, true));
			}
		}

		public static void Log(String strWrite)
		{
			strWrite = DateTime.Now + " " + strWrite;
			UtilFile.WriteToFile(strWrite, strLogFile);
		}

		public int GUIProgBar(String strAction, int intValue)
		{
			return GUIProgBar(strAction, intValue, false);
		}

		public void GUICmdState(String strCmd, String strAction)
		{
			System.Windows.Forms.Button btn = null;
			
			switch(strCmd.ToUpper())
			{	
				case "START" :
					btn = frmMain.btnStart;
					break;
				case "STOP" :
					btn = frmMain.btnStop;
					break;
				case "EXIT" :
					btn = frmMain.btnExit;
					break;
				default :
					break;
			}
			
			if(btn != null)
				if(strAction.ToUpper() == "DISABLED")
					btn.Enabled = false;
				else
					btn.Enabled = true;
		}

		public void GUIText(String strTextBox, String strText)
		{
			switch(strTextBox.ToUpper())
			{	
				case "STATUS" :
					frmMain.txtStatus.Text = strText;
					break;
				default :
					break;
			}
		}

		public static void DoEvents()
		{
			Application.DoEvents();
		}

		public static int GUIProgBar(String strAction, int intValue, bool boolGet)
		{
			//this function is for easy access to the prog bar.. uses a static variable..
			// now we can interact with the progress bar much easier (like it's not limited by 32000)
			try
			{
				switch(strAction.ToUpper())
				{
					case "MIN" :
						if(boolGet)
						{
							return frmMain.ProgressBar1.Minimum;
						}
						frmMain.ProgressBar1.Minimum = intValue;
						break;
					case "MAX" :
						if(boolGet)
						{
							return frmMain.ProgressBar1.Maximum;
						}
						frmMain.ProgressBar1.Maximum = intValue;
						break;
					case "VALUE" :
						if(boolGet)
						{
							return frmMain.ProgressBar1.Value;
						}
						frmMain.ProgressBar1.Value = intValue;
						break;
					default:
						break;
				}

				Application.DoEvents();
			}
			catch
			{
				//error... just keep going
			}
			return -1;
		}
	}
}
