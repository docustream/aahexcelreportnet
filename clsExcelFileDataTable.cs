using System;
using System.Data;
using System.Data.OleDb;

namespace dnUtil
{
	/// <summary>
	/// Instead of editing the Excel application object directly, this class will query the Excel file using ADO.Net
	/// to get a DataTable to edit. After making changes to the DataTable, saving the file will actually delete and 
	/// recreate the Excel file with the updated DataTable. This works faster than editing Excel application object
	/// row by row.
	/// </summary>
	public class clsExcelFileDataTable : clsExcelFile
	{
		protected DataTable dTblExcel;

		public clsExcelFileDataTable(string strFileName) : base(strFileName)
		{
		}

		public override bool Open() 
		{
			SetExcelDataTable(null);

			return base.Open();
		}

		public bool Open(bool blSetExcelDataTable) 
		{
			if(blSetExcelDataTable)
				SetExcelDataTable(null);

			return base.Open();
		}

		public override bool SaveFile()
		{
			Close();
			if(UtilFile.FileIsThere(strFile))
				UtilFile.KillFileConfirm(strFile);

			Open(false);
			base.AddColumnHeaders(dTblExcel.Columns);
			AddRows(dTblExcel);
			AutoFitToColumns();
			base.SaveFile();

			return true;
		}

		private string SetExcelDataTable(string strWorksheetName)
		{
			string strErr = "";

			// this must be called before xlApp object is created
			if(xlApp != null)
				return "";

			// Create new DataTable to hold information from the worksheet.
			dTblExcel = new DataTable();

			// if the file does not exist yet just return, we have an emtpy datatable
			if(!UtilFile.FileIsThere(strFile))
				return "";

			OleDbConnection cnExcelFile = null;
			OleDbDataAdapter da = null;
			
			if(strWorksheetName == null || strWorksheetName == "")
				strWorksheetName = "Sheet1";

			try
			{
				// Create connection string variable
				String strConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
					"Data Source=" + strFile + ";" + "Extended Properties=Excel 8.0;";
			
				// Create connection object by using the preceding connection string.
				cnExcelFile = new OleDbConnection(strConnectionString);
				cnExcelFile.Open();

				string SQL = "SELECT * FROM [" + strWorksheetName + "$]";
				da = new OleDbDataAdapter(SQL, cnExcelFile);
				da.Fill(dTblExcel);
			}
			catch(Exception ex)
			{
				// boo excel 97
				strErr = "ERROR - " + ex.Message;
			}
			finally
			{
				// Clean up objects.
				da.Dispose();
				da = null;
				cnExcelFile.Close();
				cnExcelFile = null;
			}

			return strErr;
		}

		public override bool AddColumnHeaders(DataColumnCollection columns)
		{
			// make sure Excel DataTable also has the same column names
			if(dTblExcel.Columns.Count > 0)
				for(int i=0; i < columns.Count; i++)
					dTblExcel.Columns[i].ColumnName = columns[i].ColumnName;  // overwrite columns if they exist
			else
				for(int i=0; i < columns.Count; i++)
					dTblExcel.Columns.Add(columns[i].ColumnName);	// add columns if they don't exist

			return base.AddColumnHeaders(columns);
		}

		public override bool AddRowsNonIdentical(DataTable dTbl, string[] arrColumnsToCheck, bool blUpdate, string[] arrColumnsToUpdate)
		{
			// we will be adding to the row after the last non-blank row
			int intAddRowIndex = GetLastNonBlankRowDT() + 1;

			foreach(DataRow dr in dTbl.Rows)
			{
				System.Windows.Forms.Application.DoEvents();
				int intRowIndex = -1;

				if(!ContainsRow(dr, arrColumnsToCheck, out intRowIndex))
				{
					DataRow drNew = dTblExcel.NewRow();
					foreach(DataColumn col in dTbl.Columns)
					{
						drNew[col.ColumnName] = dr[col.ColumnName];
					}
					dTblExcel.Rows.InsertAt(drNew, intAddRowIndex++);
					if(dTblExcel.Rows.Count > intAddRowIndex)
						dTblExcel.Rows.RemoveAt(intAddRowIndex);
				}
				else if(blUpdate)
				{
					if(arrColumnsToUpdate == null)
						UpdateRow(dr, intRowIndex, null);
					else
						UpdateRow(dr, intRowIndex, arrColumnsToUpdate);
				}
			}

			return true;
		}

		/// <summary>
		/// Updates the datatable row that has matching data in columns specified in arrColumnNames with the data from DataRow dr.
		/// </summary>
		public override bool UpdateRow(DataRow dr, int intRowIndex, string[] arrColumnsToUpdate)
		{
			if(arrColumnsToUpdate == null)
			{
				foreach(DataColumn col in dr.Table.Columns)
				{
					dTblExcel.Rows[intRowIndex][col.ColumnName] = dr[col.ColumnName];
				}
			}
			else
			{
				// only update columns specified in arrColumnsToUpdate
				for(int j = 0; j < arrColumnsToUpdate.Length; j++)
				{
					dTblExcel.Rows[intRowIndex][arrColumnsToUpdate[j]] = dr[arrColumnsToUpdate[j]];
				}
			}

			return true;
		}

		/// <summary>
		/// Gets the last non-blank row in the datatable. If the datatable is empty, this will return -1
		/// </summary>
		public int GetLastNonBlankRowDT()
		{
			// check for any blank rows that are at the end of the excel datatable
			// loop starting from end and stop when we encounter a non blank row
			int intLastNonBlankRow = dTblExcel.Rows.Count;
			bool IsBlankRow = true;

			for(int i=dTblExcel.Rows.Count-1; i >= 0; i--)
			{
				System.Windows.Forms.Application.DoEvents();
				for(int j=0; j < dTblExcel.Columns.Count; j++)
				{
					if(dTblExcel.Rows[i][j].ToString()  != "")
					{
						IsBlankRow = false;
						break;
					}
				}
				// we found a non-blank row or reached the top, save the index and break loop
				if(!IsBlankRow)
				{
					intLastNonBlankRow = i;
					break;
				}
				else if(i == 0) // first row in datatable is blank
				{
					intLastNonBlankRow = -1;
					break;
				}
			}

			return intLastNonBlankRow;
		}

		public override bool ContainsRow(DataRow dr, string[] arrColumnNames, out int intRowIndex)
		{
			for(int i=0; i < dTblExcel.Rows.Count; i++)
			{
				if(MatchingRowDT(i, dr, arrColumnNames))
				{
					intRowIndex = i;
					return true;
				}
			}

			intRowIndex = -1;
			return false;
		}

		protected bool MatchingRowDT(int intRowIndex, DataRow dr, string[] arrColumnNames)
		{
			for(int i=0; i < arrColumnNames.Length; i++)
			{
				string strToCheck = dTblExcel.Rows[intRowIndex][arrColumnNames[i]].ToString();

				// if this is a DateTime object, we need to properly convert it to a short string for comparison
				if(dTblExcel.Rows[intRowIndex][arrColumnNames[i]].GetType() == Type.GetType("System.DateTime"))
					 strToCheck = ((DateTime)dTblExcel.Rows[intRowIndex][arrColumnNames[i]]).ToShortDateString();

				if(strToCheck != dr[arrColumnNames[i]].ToString())
					return false;
			}

			return true;
		}
	}
}
