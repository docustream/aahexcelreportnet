using System;
using System.Data;
using Excel = Microsoft.Office.Interop.Excel;

namespace dnUtil
{
	/// <summary>
	/// Summary description for test.
	/// </summary>
	public class test
	{
		public test()
		{
		}

		public void PrintToExcel(DataSet ds_PrintToExcel)
		{
			Excel.Application excel = new Excel.Application();
			Excel.Worksheet ws = Activate(excel);

			if(ws != null)
			{
				FillColumnHeaders(ws, ds_PrintToExcel);
				FillDataRows(ws, ds_PrintToExcel);
				SetAutoFitToColumns(ws,ds_PrintToExcel);
				excel.Visible = true;
			}
		}

		private void FillColumnHeaders(Excel.Worksheet ws, DataSet ds_PrintToExcel)
		{
			int colcount = 1;
			foreach (DataColumn dc in ds_PrintToExcel.Tables[0].Columns)
			{
				string nextItem = dc.ColumnName;
				AddItemToSpreadsheet(1, colcount, ws, nextItem);

				if (dc.DataType == System.Type.GetType("System.DateTime"))
				{
					// format for date time in excel
					FormatColumn(ws, colcount, "mmm-d-yyyy hh:mm:ss.000");
					// set the column width in excel
					SetColumnWidth(ws, colcount, 25);
				}
				colcount++;
			}
			BoldRow(1, ws);
		}

		private void FillDataRows(Excel.Worksheet ws, DataSet ds_PrintToExcel)
		{
			int rowcount = 2;
			int colcount = 1;

			foreach (DataRow dr in ds_PrintToExcel.Tables[0].Rows)
			{
				colcount = 1;
				object[] items = dr.ItemArray;

				foreach (object o in items)
				{
					string nextItem = "";

					// if the data is date time, format the data
					if (o is DateTime)
					{
						nextItem = ((DateTime)o).ToString("MMM-d-yyyy hh:mm:ss.fff");
					}

					else
					{
						nextItem = "'" + o.ToString();
					}

					AddItemToSpreadsheet(rowcount, colcount, ws, nextItem);
					colcount++;
				}

				rowcount++;
			}
		}

		private void SetAutoFitToColumns(Excel.Worksheet ws, DataSet ds_PrintToExcel)
		{
			int totColCount = ds_PrintToExcel.Tables[0].Columns.Count;

			for(int colcount = 1; colcount <= totColCount; colcount++)
			{
				AutoFitColumn(ws, colcount);
			}
		}

		public Excel.Worksheet Activate(Excel.Application excel)
		{
			// open new excel spreadsheet
			Excel.Workbook workbook = excel.Workbooks.Add(Type.Missing);
			Excel.Worksheet ws = (Excel.Worksheet)excel.ActiveSheet;
			ws.Activate();

			return ws;
		}

		public void AddItemToSpreadsheet(int row, int column, Excel.Worksheet ws, string item)
		{
			((Excel.Range)ws.Cells[row, column]).Value2 = item;
		}

		public void BoldRow(int row, Excel.Worksheet ws)
		{
			((Excel.Range)ws.Cells[row, 1]).EntireRow.Font.Bold = true;
		}

		public void ColorRow(int row, int col, Excel.Worksheet ws)
		{
			((Excel.Range)ws.Cells[row, col]).Interior.Color = System.Drawing.Color.FromArgb(255,120,120).ToArgb();
		}

		public void ColorAltRow(int row, int col, Excel.Worksheet ws)
		{
			((Excel.Range)ws.Cells[row, col]).Interior.Color = System.Drawing.Color.FromArgb(255,180,180).ToArgb();
		}

		public void CellOutLine(int row, int col, Excel.Worksheet ws)
		{
			((Excel.Range)ws.Cells[row, col]).Borders.ColorIndex = 1;
		}

		public void FormatColumn(Excel.Worksheet ws, int col, string format)
		{
			((Excel.Range)ws.Cells[1, col]).EntireColumn.NumberFormat = format;
		}

		public void FormatColumnText(Excel.Worksheet ws, int col)
		{
			((Excel.Range)ws.Cells[1, col]).EntireColumn.NumberFormat = "@";
		}

		public void SetColumnWidth(Excel.Worksheet ws, int col, int width)
		{
			((Excel.Range)ws.Cells[1, col]).EntireColumn.ColumnWidth = width;
		}

		public void AutoFitColumn(Excel.Worksheet ws, int col)
		{
			((Excel.Range)ws.Cells[1, col]).EntireColumn.AutoFit();
		}
	}
}
