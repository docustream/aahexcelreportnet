using System;
using System.Collections;
using System.IO;

namespace dnUtil
{
	/// <summary>
	/// An in memory representation of a directory
	/// </summary>
	public class clsDirectory 
	{
		public String strDirectory;
		public ArrayList arrFile;
		public ArrayList arrDirectory;

		public clsDirectory(String strDir)
		{
			// TODO: Add constructor logic here
			strDirectory = strDir;
			arrFile = new ArrayList();
			arrDirectory = new ArrayList();
		}

		/// <summary>
		/// Populate the internal list of files and directories (but not the contents of the subdirectories)
		/// </summary>
		/// <returns></returns>
		public virtual bool Populate()
		{
			return Populate(false);
		}

		/// <summary>
		/// Populate the internal list of files and directories
		/// </summary>
		/// <param name="boolPopulateSubDirs"></param>
		/// <returns></returns>
		public virtual bool Populate(bool boolPopulateSubDirs)
		{
			Array strFiles;

			if(!UtilFile.DirIsThere(strDirectory))
			{
				return false;
			}
			
			System.Windows.Forms.Application.DoEvents();
			strFiles = System.IO.Directory.GetFiles(strDirectory);

			clsDirectory cDir;
			clsFile cFile;

			foreach(String str in strFiles)
			{
				cFile = new clsFile(str);
				arrFile.Add(cFile);
			}

			strFiles = null;
			System.Windows.Forms.Application.DoEvents();
			strFiles = System.IO.Directory.GetDirectories(strDirectory);

			System.Windows.Forms.Application.DoEvents();
			foreach(String str in strFiles)
			{
				if(str != ".." && str != ".")
				{
					//we have a directory
					cDir = new clsDirectory(str);
					if(boolPopulateSubDirs)
					{
						System.Windows.Forms.Application.DoEvents();
						cDir.Populate(true);
					}
					arrDirectory.Add(cDir);
				}
			}

			return true;
		}

		/// <summary>
		/// populate an array of matching files (clsFile objects) with all files from all 
		/// directories (including sub directories if desired)
		/// </summary>
		/// <param name="arrF">the array which to add the clsFile objects</param>
		/// <param name="strExtension">the extension of the file (not including a '.')</param>
		/// <param name="boolSubDirs"></param>
		/// <returns></returns>
		public bool GetArrFile(ref ArrayList arrF, String strExtension, bool boolSubDirs)
		{
			return GetArrFile(ref arrF, strExtension, boolSubDirs, false);
		}
		
		/// <summary>
		/// populate an array of matching files with all files from all directories (including sub directories if desired)
		/// </summary>
		/// <param name="arrF">the array which to add the string file paths to, or the clsFile objects</param>
		/// <param name="strExtension">the extension of the file (not including a '.')</param>
		/// <param name="boolSubDirs"></param>
		/// <param name="boolReturnStrings">specify whether to return the string file paths, or the clsFile objects</param>
		/// <returns></returns>
		public bool GetArrFile(ref ArrayList arrF, String strExtension, bool boolSubDirs, bool boolReturnStrings)
		{
			//'populate an array of matching files with all files from all directories (including sub dirs)
			foreach(clsFile cFile in arrFile)
			{
				if(strExtension == "")
				{	
					if(boolReturnStrings)
					{
						arrF.Add(cFile.strFile);
					}else{
						arrF.Add(cFile);	
					}
				}
				else if(UtilString.LikeStr(cFile.strFile.ToUpper(), "*." + strExtension.ToUpper()))
				{
					if(boolReturnStrings)
					{
						arrF.Add(cFile.strFile);
					}
					else
					{
						arrF.Add(cFile);
					}
				}
			}
			if(boolSubDirs)
			{
				foreach(clsDirectory cDir in arrDirectory)
				{
					if(!cDir.GetArrFile(ref arrF, strExtension, true, boolReturnStrings))
					{
						return false;
					}
				}
			}

			return true;
		}
		
		/// <summary>
		/// Get the containing directories (clsDirectory objects)
		/// </summary>
		/// <param name="arrD"></param>
		/// <returns></returns>
		public bool GetArrDir(ref ArrayList arrD)
		{
			return GetArrDir(ref arrD, false);
		}
		
		/// <summary>
		/// Get the containing directories (clsDirectory objects)
		/// </summary>
		/// <param name="arrD"></param>
		/// <param name="boolSubD"></param>
		/// <returns></returns>
		public bool GetArrDir(ref ArrayList arrD, bool boolSubDirs)
		{
			if(!boolSubDirs)
			{
				arrD = (ArrayList)arrDirectory.Clone();
				return true;
			}
			
			foreach(clsDirectory cDir in arrDirectory)
			{
				//'add the directory itself, then all it's subdirs
				arrD.Add(cDir);
				cDir.GetArrDir(ref arrD, true);
			}
		    
			return true;
		}
	}
}
