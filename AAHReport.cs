using System;
using System.Collections;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
namespace dnUtil
{
	/// <summary>
	/// Summary description for BatchStatusReport.
	/// </summary>
	public class AAHReport
	{
		public GUITextFunction GUIMessage = null;
		public GUIProgBarFunction GUIProgBarMessage = null;
		public LogFunction LogMessage = null;
		
		public AAHReport()
		{
		}
		
		/// <summary>
		/// Generates a tab delimited file and then converts it to Excel format
		/// </summary>
		public string GenerateReportTabbedToExcel(string strOutFile, DataTable dTbl)
		{
			string strStatus = "";

            // v1.0.5 -- default to xlsx
            string strTabbedFile = UtilFile.NameMinusExtension(strOutFile) + "_tabbed.xlsx";
			UtilFile.KillFileConfirm(strTabbedFile);

			StringBuilder sb = new StringBuilder(1000000); // a meg should be enough
			GUIText("STATUS", "Generating Report...");
			GUIProgBar("MAX", dTbl.Rows.Count);
			GUIProgBar("VALUE", 0);
			int i = 0;

			// we are creating a tab delimited file with .xls extension - opens up in excel just the same
			string strColumnHeaders = "";
			foreach(DataColumn col in dTbl.Columns)
			{
				strColumnHeaders += col.ColumnName + "\t ";
			}

			sb.Append(strColumnHeaders.Substring(0, strColumnHeaders.Length-2) + "\r\n");
			
			foreach(DataRow dRow in dTbl.Rows)
			{
				GUIProgBar("VALUE", i++);

				string strBatchLine = "";
				foreach(DataColumn col in dTbl.Columns)
				{
					strBatchLine += dRow[col.ColumnName].ToString() + "\t ";
				}

				sb.Append(strBatchLine.Substring(0, strBatchLine.Length-2) + "\r\n");
			}
			
			// write tab delimited file
			UtilFile.WriteToFile(sb.ToString(), strTabbedFile);

			// convert tab delimited file to excel format
            strStatus = TabDelimToExcel(strTabbedFile, strOutFile);
			if(strStatus == "")
				strStatus = "FINISHED";

			GUIText("STATUS", "Done Generating Report");
			GUIProgBar("VALUE", 0);
			
			return strStatus;
		}

		/// <summary>
		/// Creates an Excel file from an existing tab delimited file
		/// </summary>
		private string TabDelimToExcel(string strTabDelimFile, string strOutFile)
		{
			UtilFile.KillFileConfirm(strOutFile);

			object objMissing = System.Reflection.Missing.Value;
			Excel.Application xlApp = null;
			Excel.Workbooks wbsBooks = null;
			Excel._Workbook wbBook = null;

			try
			{
				// Open the text file in Excel.
				xlApp = new Excel.Application();
				wbsBooks = (Excel.Workbooks)xlApp.Workbooks;
				wbsBooks.OpenText(strTabDelimFile, Excel.XlPlatform.xlWindows, 1, 
					Excel.XlTextParsingType.xlDelimited, Excel.XlTextQualifier.xlTextQualifierDoubleQuote,
					false, true, false, false, false, false, false, objMissing, 
					objMissing);

				wbBook = xlApp.ActiveWorkbook;

                // Save the text file in the typical workbook format and quit Excel.
                // v1.0.5 -- updated to save as xlsx; xlOpenXMLWorkbook
                wbBook.SaveAs(strOutFile, Excel.XlFileFormat.xlOpenXMLWorkbook, 
					objMissing, objMissing, objMissing, objMissing, Excel.XlSaveAsAccessMode.xlNoChange, objMissing, objMissing,
					objMissing, objMissing);
			}
			catch(Exception ex)
			{
				// boo excel 97
				Log(ex.Message);
				return "ERROR";
			}
			finally
			{
				// close and release Excel resources -- GC will need to be called outside scope of this function
				if(wbBook != null)
				{
					wbBook.Close(false, objMissing, objMissing);
					System.Runtime.InteropServices.Marshal.ReleaseComObject(wbBook);
					wbBook = null;
				}
				if(xlApp != null)
				{
					xlApp.Quit();
					System.Runtime.InteropServices.Marshal.ReleaseComObject(xlApp);
					xlApp = null;
				}
			}

			return "";
		}

		/// <summary>
		/// Generates an Excel file populated with data from DataTable dTbl
		/// </summary>
		public string GenerateReportExcel(string strOutFile, DataTable dTbl)
		{
			string strStatus = "";

			GUIText("STATUS", "Generating Report...");
			GUIProgBar("MAX", 100);
			GUIProgBar("VALUE", 0);
			
			clsExcelFileDataTableAAH cExcelReport = null;

			try
			{
				// create excel file object
				cExcelReport = new clsExcelFileDataTableAAH(strOutFile);
				cExcelReport.Open();

				cExcelReport.LogMessage = new LogFunction(Log);
				cExcelReport.GUIMessage = new GUITextFunction(GUIText);
				cExcelReport.GUIProgBarMessage = new GUIProgBarFunction(GUIProgBar);

				// if column headers do not exist, add them and adjust column widths
				if(!cExcelReport.ColumnHeadersExist(dTbl.Columns))
				{
					cExcelReport.AddColumnHeaders(dTbl.Columns);
					cExcelReport.AutoFitToColumns();
					//cExcelReport.AdjustColumnWidths(new int[] {25, 12, 12, 12, 12, 12, 12, 12});
				}

				string[] arrColumnsToCheck = new string[] {"BatchName", "ReceivedDate"};
				string[] arrColumnsToUpdate = new string[] {"ImageCount", "ClaimCount"};

				// add non identical rows from dTbl, update identical ones
				cExcelReport.AddRowsNonIdentical(dTbl, arrColumnsToCheck, true, arrColumnsToUpdate);
				
				GUIText("STATUS", "Saving...");
				cExcelReport.SaveFile();
			}
			catch(System.IO.FileNotFoundException ex)
			{
				Log(ex.Message);
				MessageBox.Show("Please make sure the file 'Interop.Excel.dll' is in the program path",
					"FileNotFoundException", MessageBoxButtons.OK, MessageBoxIcon.Error);
				strStatus = "ERROR";
			}
			catch(Exception ex)
			{
				// boo excel 97
				Log(ex.Message);
				strStatus = "ERROR";
			}
			finally
			{
				// close the excel file
				if(cExcelReport != null)
					cExcelReport.Close();
				cExcelReport = null;
			}

			// garbage collection MUST be called to release excel object
			// also this must be called outside of scope of the Excel.Application object
			// -- otherwise stupid excel will still run in the background until app closes
			GC.Collect();
			GC.WaitForPendingFinalizers();
			GC.Collect();

			if(strStatus == "")
				strStatus = "FINISHED";

			GUIText("STATUS", "Done Generating Report");
			GUIProgBar("VALUE", 0);
			Application.DoEvents();
			
			return strStatus;
		}

		/// <summary>
		/// Don't use
		/// </summary>
		private string CreateNewReport(string strOutFile, DataTable dTbl)
		{
			UtilFile.KillFileConfirm(strOutFile);

			string strStatus = "";

			object objMissing = System.Reflection.Missing.Value;
			Excel.Application xlApp = null;
			Excel.Workbooks wbsBooks = null;
			Excel._Workbook wbBook = null;
			Excel.Sheets shSheets = null;
			Excel._Worksheet wsSheet = null;
			Excel.Range rngRange = null;
			Excel.Font xlFont = null;

			try
			{
				xlApp = new Excel.Application();
				wbsBooks = (Excel.Workbooks)xlApp.Workbooks;
				wbBook = (Excel._Workbook)(wbsBooks.Add(objMissing));

				// we will add data to the first worksheet in the new workbook
				shSheets = (Excel.Sheets)wbBook.Worksheets;
				wsSheet = (Excel._Worksheet)(shSheets.get_Item(1));

				GUIProgBar("VALUE", 20);

				// add column names to first row in worksheet (starting at cell A1)
				object[] arrColumnNames = new object[dTbl.Columns.Count];
				for(int i=0; i < arrColumnNames.Length; i++)
				{
					arrColumnNames[i] = dTbl.Columns[i].ColumnName;
				}
				rngRange = wsSheet.get_Range("A1", objMissing);
				rngRange = rngRange.get_Resize(1, arrColumnNames.Length);
				rngRange.Value = arrColumnNames;

				// make the columns bold
				xlFont = rngRange.Font;
				xlFont.Bold = true;

				// adjust column widths 
				rngRange.ColumnWidth = 10;
				// make 1st column (BatchName) a little longer than the rest
				rngRange = wsSheet.get_Range("A1", objMissing);
				rngRange.ColumnWidth = 25;

				GUIProgBar("VALUE", 60);

				// create a 2D array from the datatable and add it to the worksheet starting at cell A2
				object[,] arr2dData = UtilDB.DataTableToArray2D(dTbl);
				rngRange = wsSheet.get_Range("A2", objMissing);
				rngRange = rngRange.get_Resize(dTbl.Rows.Count, dTbl.Columns.Count);
				rngRange.Value = arr2dData;

				// save the workbook to file
                // v1.0.5 -- updated to save as xlsx; "xlOpenXMLWorkbook"
				wbBook.SaveAs(strOutFile, Excel.XlFileFormat.xlOpenXMLWorkbook, objMissing,
                    objMissing, objMissing, objMissing, Excel.XlSaveAsAccessMode.xlNoChange, 
					objMissing, objMissing, objMissing, objMissing);

				GUIProgBar("VALUE", 100);
			}
			catch(Exception ex)
			{
				// boo excel 97
				Log(ex.Message);
				strStatus = "ERROR";
			}
			finally
			{
				// close and release Excel resources -- GC will need to be called outside scope of this method
				if(wbBook != null)
				{
					wbBook.Close(false, objMissing, objMissing);
					System.Runtime.InteropServices.Marshal.ReleaseComObject(wbBook);
					wbBook = null;
				}
				if(xlApp != null)
				{
					xlApp.Quit();
					System.Runtime.InteropServices.Marshal.ReleaseComObject(xlApp);
					xlApp = null;
				}
			}
            
			return strStatus;
		}

		/// <summary>
		/// Don't use
		/// </summary>
		public string AppendReportNew(string strOutFile, DataTable dTbl)
		{
			string strStatus = "";

			object objMissing = System.Reflection.Missing.Value;
			Excel.Application xlApp = null;
			Excel.Workbooks wbsBooks = null;
			Excel._Workbook wbBook = null;
			Excel.Sheets shSheets = null;
			Excel._Worksheet wsSheet = null;
			Excel.Range rngRange = null;
			Excel.Range rngEndRange = null;

			try
			{
				// Open the text file in Excel.
				xlApp = new Excel.Application();
				wbsBooks = (Excel.Workbooks)xlApp.Workbooks;
				wbBook = wbsBooks.Open(strOutFile, 0, false, 5, "", "", false, Excel.XlPlatform.xlWindows, "\t", true, false, 0, true);
				shSheets = wbBook.Worksheets;
				wsSheet = (Excel.Worksheet)shSheets.get_Item(1);

				// check for any blank rows that are at the end of the excel datatable
				// loop starting from end and stop when we encounter a non blank row
				rngRange = wsSheet.UsedRange;
				int intLastNonBlankRow = rngRange.Rows.Count;
				rngEndRange = wsSheet.get_Range("A" + intLastNonBlankRow, objMissing);
				bool IsBlankRow = true;

				for(int i=rngRange.Rows.Count; i > 0; i--)
				{
					for(int j=1; j < rngRange.Columns.Count; j++)
					{
						rngEndRange = wsSheet.get_Range(ToAlphaColumn(j) + i.ToString(), objMissing);
						string strCell = rngEndRange.Text.ToString();
						
						if(strCell != "")
						{
							IsBlankRow = false;
							break;
						}
					}
					// we found a non-blank row or reached the top, save the index and break loop
					if(!IsBlankRow || i == 1)
					{
						intLastNonBlankRow = i;
						break;
					}
				}

				// create a 2D array from the datatable and add it to the worksheet starting at the row after the last non blank one
				// -- if the worksheet is blank, this will leave one row open for the column headers
				object[,] arr2dData = UtilDB.DataTableToArray2D(dTbl);
				rngRange = wsSheet.get_Range("A" + (intLastNonBlankRow + 1).ToString(), objMissing);
				rngRange = rngRange.get_Resize(dTbl.Rows.Count, dTbl.Columns.Count);
				rngRange.Value = arr2dData;

				// Save the workbook
				wbBook.Save();
			}
			catch(Exception ex)
			{
				// boo excel 97
				Log(ex.Message);
				return "ERROR";
			}
			finally
			{
				// close and release Excel resources -- GC will need to be called outside scope of this method
				if(wbBook != null)
				{
					wbBook.Close(false, objMissing, objMissing);
					System.Runtime.InteropServices.Marshal.ReleaseComObject(wbBook);
					wbBook = null;
				}
				if(xlApp != null)
				{
					xlApp.Quit();
					System.Runtime.InteropServices.Marshal.ReleaseComObject(xlApp);
					xlApp = null;
				}
			}

			if(strStatus == "")
				strStatus = "FINISHED";

			return strStatus;
		}

		/// <summary>
		/// Don't use
		/// </summary>
		public string AppendReportOld(string strOutFile, DataTable dTbl)
		{
			string strStatus = "";

			OleDbConnection cnExcelFile = null;
			OleDbDataAdapter da = null;

			try
			{
				// Create connection string variable. Modify the "Data Source"
				// parameter as appropriate for your environment.
				String strConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" +
					"Data Source=" + strOutFile + ";" + "Extended Properties=Excel 8.0;";
			
				// Create connection object by using the preceding connection string.
				cnExcelFile = new OleDbConnection(strConnectionString);
				cnExcelFile.Open();

				// Create new DataTable to hold information from the worksheet.
				DataTable dTblExcel = new DataTable();

				string SQL = "SELECT * FROM [Sheet1$]";
				da = new OleDbDataAdapter(SQL, cnExcelFile);
				da.Fill(dTblExcel);

				// copy the rows from the current report datatable as new rows in the excel datatable
				foreach(DataRow dr in dTbl.Rows)
				{
					// if the excel worksheet already contains this batch, continue
					if(UtilDB.DataTableContains(dTblExcel, dr["BatchName"].ToString(), "BatchName"))
						continue;

					DataRow drNew = dTblExcel.NewRow();
					foreach(DataColumn col in dTbl.Columns)
					{
						drNew[col.ColumnName] = dr[col.ColumnName];
					}
					dTblExcel.Rows.Add(drNew);
				}

				da.Update(dTblExcel);
			}
			catch(Exception ex)
			{
				// boo excel 97
				Log(ex.Message);
				strStatus = "ERROR";
			}
			finally
			{
				// Clean up objects.
				da.Dispose();
				cnExcelFile.Close();
			}

			if(strStatus == "")
				strStatus = "FINISHED";

			return strStatus;
		}

		/// <summary>
		/// Converts a 1-based column index to excel's alpha format (A, B, C, ... AA, AB, AC...)
		/// -- The maximum returned is index returned is ZZ
		/// </summary>
		private string ToAlphaColumn(int intColumn)
		{
			string strRet = "";
			int intMaxCols = 701;	// 26 + (26 * 26) - 1;
			if(intColumn >= intMaxCols)
				return "ZZ";

			intColumn--;
			int intDigit1 = intColumn / 26;
			int intDigit2 = intColumn % 26;
			char char1 = Convert.ToChar(65 + intDigit1 - 1);
			char char2 = Convert.ToChar(65 + intDigit2);

			if(intDigit1 > 0)
				strRet = char1.ToString();

			strRet += char2.ToString();

			return strRet;
		}

		protected void Log(string strMessage)
		{
			if(LogMessage != null)
			{
				LogMessage(strMessage);
			}
		}

		protected void GUIText(string strTextBox, string strMessage)
		{
			if(GUIMessage != null)
			{
				GUIMessage(strTextBox, strMessage);
			}
		}
		
		protected int GUIProgBar(string strAction, int intValue)
		{
			if(GUIProgBarMessage != null)
			{
				return GUIProgBarMessage(strAction, intValue);
			}
			return 0;
		}
	}
}
