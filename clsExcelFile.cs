using System;
using System.Data;
using System.Data.OleDb;

using Excel = Microsoft.Office.Interop.Excel;
namespace dnUtil
{
	/// <summary>
	/// Summary description for clsExcelFile.
	/// </summary>
	public class clsExcelFile
	{
		protected string strFile = "";
		protected object objMissing = System.Reflection.Missing.Value;
		protected Excel.Application xlApp = null;
		protected Excel.Workbooks wbsBooks = null;
		protected Excel._Workbook wbBook = null;
		protected Excel.Sheets shSheets = null;
		protected Excel._Worksheet wsSheet = null;
		
		public string FileName
		{
			get{return strFile;}
		}

		public clsExcelFile(string strFileName)
		{
			strFile = strFileName;
		}

		public virtual bool Open()
		{
			if(!UtilFile.FileIsThere(strFile))
				return OpenNew();

			xlApp = new Excel.Application();
			wbsBooks = (Excel.Workbooks)xlApp.Workbooks;
			wbBook = wbsBooks.Open(strFile, 0, false, 5, "", "", false, Excel.XlPlatform.xlWindows, "\t", true, false, 0, true);
			shSheets = wbBook.Worksheets;

			SetWorkSheet(1);

			return true;
		}

		private bool OpenNew()
		{
			xlApp = new Excel.Application();
			wbsBooks = (Excel.Workbooks)xlApp.Workbooks;
			wbBook = (Excel._Workbook)(wbsBooks.Add(objMissing));
			shSheets = (Excel.Sheets)wbBook.Worksheets;

			SetWorkSheet(1);

			return true;
		}

		/// <summary>
		/// Close and release Excel resources.
		/// NOTE: GC will need to be called outside scope of this object
		/// </summary>
		public virtual bool Close()
		{
			// close and release Excel resources -- GC will need to be called outside scope of this object
			if(wbBook != null)
			{
				wbBook.Close(false, objMissing, objMissing);
				System.Runtime.InteropServices.Marshal.ReleaseComObject(wbBook);
				wbBook = null;
			}
			if(xlApp != null)
			{
				xlApp.Quit();
				System.Runtime.InteropServices.Marshal.ReleaseComObject(xlApp);
				xlApp = null;
			}

			return true;
		}

		public bool SetWorkSheet(int intWorkSheetIndex)
		{
			if(intWorkSheetIndex > shSheets.Count)
				return false;

			wsSheet = (Excel._Worksheet)(shSheets.get_Item(intWorkSheetIndex));
			wsSheet.Activate();

			return true;
		}

		public bool CreateNewWorksheet()
		{
			Excel.Worksheet newWorksheet;
			newWorksheet = (Excel.Worksheet)shSheets.Add(objMissing, objMissing, objMissing, objMissing);
			newWorksheet.Activate();

			return true;
		}

		public bool RenameWorkSheet(string strName)
		{
			wsSheet.Name = strName;

			return true;
		}

		public virtual bool ColumnHeadersExist()
		{
			Excel.Range rngRange;
			for(int i=1; i <= wsSheet.UsedRange.Columns.Count; i++)
			{
				rngRange = wsSheet.get_Range(ToAlphaColumn(i) + "1", objMissing);
				
				if(rngRange.Text.ToString() != "")
					return true;
			}

			return false;
		}

		public virtual bool ColumnHeadersExist(DataColumnCollection columns)
		{
			Excel.Range rngRange;
			for(int i=0; i < columns.Count; i++)
			{
				rngRange = wsSheet.get_Range(ToAlphaColumn(i+1) + "1", objMissing);
				
				if(rngRange.Text.ToString() != columns[i].ColumnName)
					return false;
			}

			return true;
		}

		public virtual bool AddColumnHeaders(DataColumnCollection columns)
		{
			// add column names to first row in worksheet (starting at cell A1)
			object[] arrColumnNames = new object[columns.Count];
			for(int i=0; i < arrColumnNames.Length; i++)
			{
				arrColumnNames[i] = columns[i].ColumnName;
			}
			Excel.Range rngRange = wsSheet.get_Range("A1", objMissing);
			rngRange = rngRange.get_Resize(1, arrColumnNames.Length);
			rngRange.Value = arrColumnNames;

			// make the columns bold
			Excel.Font xlFont = rngRange.Font;
			xlFont.Bold = true;

			return true;
		}

		public bool AdjustColumnWidths(int[] arrWidths)
		{
			Excel.Range rngRange;

			for(int intCol=0; intCol < arrWidths.Length; intCol++)
			{
				rngRange = wsSheet.get_Range(ToAlphaColumn(intCol+1) + "1", objMissing);
				rngRange.ColumnWidth = arrWidths[intCol];
			}

			return true;
		}

		public void AutoFitToColumns()
		{
			int intColCount = wsSheet.UsedRange.Columns.Count;

			for(int intCol = 1; intCol <= intColCount; intCol++)
			{
				AutoFitColumn(intCol);
			}
		}

		public void AutoFitColumn(int intCol)
		{
			((Excel.Range)wsSheet.Cells[1, intCol]).EntireColumn.AutoFit();
		}

		/// <summary>
		/// Returns the 1-based column index of the specified column. Returns -1 if not found.
		/// </summary>
		public int GetColumnIndex(string strColumnName)
		{
			Excel.Range rngRange;

			for(int i=1; i <= wsSheet.UsedRange.Columns.Count; i++)
			{
				rngRange = ((Excel.Range)wsSheet.Cells[1, i]);
				string strCell = rngRange.Text.ToString();
						
				if(strCell == strColumnName)
					return i;
			}

			return -1;
		}

		public virtual bool AddRows(DataTable dTbl)
		{
			int intAddRowIndex = GetLastNonBlankRow() + 1;

			// create a 2D array from the datatable and add it to the worksheet starting at the row after the last non blank one
			object[,] arr2dData = UtilDB.DataTableToArray2D(dTbl);
			Excel.Range rngRange = wsSheet.get_Range("A" + intAddRowIndex.ToString(), objMissing);
			rngRange = rngRange.get_Resize(dTbl.Rows.Count, dTbl.Columns.Count);
			rngRange.Value = arr2dData;

			return true;
		}

		/// <summary>
		/// Adds rows from dTbl to the worksheet. Checks if each column of a row is identical to an already 
		/// existing row in the worksheet. Must be careful of how Excel reformats data (ex. the string 07/07/2007 is entered 
		/// into Excel as 7/7/2007 by default).
		/// </summary>
		public virtual bool AddRowsNonIdentical(DataTable dTbl)
		{
			foreach(DataRow dr in dTbl.Rows)
			{
				System.Windows.Forms.Application.DoEvents();
				if(!ContainsRow(dr))
					AddRow(dr);
			}

			return true;
		}

		/// <summary>
		/// Adds rows from dTbl to the worksheet. If there is already a row with the same key data, it will update that row if
		/// specified to do so with blUpdate. Only updates the columns specified in arrColumnsToUpdate. If arrColumnsToUpdate 
		/// is null, the whole row is updated.
		/// </summary>
		public virtual bool AddRowsNonIdentical(DataTable dTbl, string strKeyColumn, bool blUpdate, string[] arrColumnsToUpdate)
		{
			foreach(DataRow dr in dTbl.Rows)
			{
				System.Windows.Forms.Application.DoEvents();
				if(!ContainsRow(dr[strKeyColumn].ToString(), strKeyColumn))
					AddRow(dr);
				else if(blUpdate)
				{
					if(arrColumnsToUpdate == null)
						UpdateRow(dr, strKeyColumn, null);
					else
						UpdateRow(dr, strKeyColumn, arrColumnsToUpdate);
				}
			}

			return true;
		}

		/// <summary>
		/// Adds rows from dTbl to the worksheet. Instead of using a key column, this checks for matching data in the columns
		/// in arrColumnsToCheck.
		/// </summary>
		public virtual bool AddRowsNonIdentical(DataTable dTbl, string[] arrColumnsToCheck, bool blUpdate, string[] arrColumnsToUpdate)
		{
			foreach(DataRow dr in dTbl.Rows)
			{
				System.Windows.Forms.Application.DoEvents();
				int intRowIndex = -1;
				if(!ContainsRow(dr, arrColumnsToCheck, out intRowIndex))
					AddRow(dr);
				else if(blUpdate)
				{
					if(arrColumnsToUpdate == null)
						UpdateRow(dr, intRowIndex, null);
					else
						UpdateRow(dr, intRowIndex, arrColumnsToUpdate);
				}
			}

			return true;
		}

		public virtual bool AddRow(DataRow dr)
		{
			int intAddRowIndex = GetLastNonBlankRow() + 1;

			object[] arrData = dr.ItemArray;
			Excel.Range rngRange = wsSheet.get_Range("A" + intAddRowIndex.ToString(), objMissing);
			rngRange = rngRange.get_Resize(1, arrData.Length);
			rngRange.Value = arrData;
			// could also do rngRange.EntireRow.Insert then add data
			return true;
		}

		/// <summary>
		/// Updates the worksheet row that has matching data in columns specified in arrColumnNames with the data from DataRow dr.
		/// </summary>
		public virtual bool UpdateRow(DataRow dr, int intRowIndex, string[] arrColumnsToUpdate)
		{
			object[] arrData = dr.ItemArray;
			Excel.Range rngRange;

			if(arrColumnsToUpdate == null)
			{
				rngRange = wsSheet.get_Range("A" + intRowIndex.ToString(), objMissing);
				rngRange = rngRange.get_Resize(1, arrData.Length); // resize range to include entire row
				rngRange.Value = arrData;
			}
			else
			{
				// only update columns specified in arrColumnsToUpdate
				for(int j = 0; j < arrColumnsToUpdate.Length; j++)
				{
					System.Windows.Forms.Application.DoEvents();
					UpdateCell(intRowIndex, GetColumnIndex(arrColumnsToUpdate[j]), dr[arrColumnsToUpdate[j]].ToString());
				}
			}

			return true;
		}

		/// <summary>
		/// Updates the worksheet row that has matching key data with the data in DataRow dr. Only updates the columns
		/// specified in arrColumnsToUpdate.
		/// </summary>
		public virtual bool UpdateRow(DataRow dr, string strKeyColumnName, string[] arrColumnsToUpdate)
		{
			int intKeyIndex = GetColumnIndex(strKeyColumnName);
			string strKeyIndex = ToAlphaColumn(intKeyIndex);
			string strKeyData = dr[strKeyColumnName].ToString();
			object[] arrData = dr.ItemArray;
			Excel.Range rngRange;
			bool blRet = false;

			for(int i=1; i <= wsSheet.UsedRange.Rows.Count; i++)
			{
				System.Windows.Forms.Application.DoEvents();
				rngRange = wsSheet.get_Range(strKeyIndex + i.ToString(), objMissing);
				string strCell = rngRange.Text.ToString();
					
				if(strCell == strKeyData)
				{
					if(arrColumnsToUpdate == null)
					{
						// resize range to include entire row
						rngRange = wsSheet.get_Range("A" + i.ToString(), objMissing);
						rngRange = rngRange.get_Resize(1, arrData.Length);
						rngRange.Value = arrData;
					}
					else
					{
						// only update columns specified in arrColumnsToUpdate
						for(int j = 0; j < arrColumnsToUpdate.Length; j++)
						{
							System.Windows.Forms.Application.DoEvents();
							UpdateCell(i, GetColumnIndex(arrColumnsToUpdate[j]), dr[arrColumnsToUpdate[j]].ToString());
						}
					}

					blRet = true;
					break;
				}
			}

			return blRet;
		}
		
		public virtual void UpdateCell(int intRow, int intColumn, string strData)
		{
			Excel.Range rngRange = ((Excel.Range)wsSheet.Cells[intRow, intColumn]);
			rngRange.Value = strData;
		}

		public virtual void UpdateCell(string strAlphaColumnRow, string strData)
		{
			Excel.Range rngRange = wsSheet.get_Range(strAlphaColumnRow, objMissing);
			rngRange.Value = strData;
		}

		/// <summary>
		/// Gets the last non-blank row in the worksheet. If the worksheet is blank, this will return 1 (the first row).
		/// This will always leave the top row open for column headers.
		/// </summary>
		public int GetLastNonBlankRow()
		{
			// check for any blank rows that are at the end of the excel worksheet
			// loop starting from end and stop when we encounter a non blank row
			Excel.Range rngRange = wsSheet.UsedRange;
			int intLastNonBlankRow = rngRange.Rows.Count;
			Excel.Range rngEndRange = wsSheet.get_Range("A" + intLastNonBlankRow, objMissing);
			bool IsBlankRow = true;

			for(int i=rngRange.Rows.Count; i > 0; i--)
			{
				System.Windows.Forms.Application.DoEvents();

				for(int j=1; j <= rngRange.Columns.Count; j++)
				{
					rngEndRange = ((Excel.Range)wsSheet.Cells[i, j]);
					string strCell = rngEndRange.Text.ToString();
						
					if(strCell != "")
					{
						IsBlankRow = false;
						break;
					}
				}
				// we found a non-blank row or reached the top, save the index and break loop
				if(!IsBlankRow || i == 1)
				{
					intLastNonBlankRow = i;
					break;
				}
			}

			return intLastNonBlankRow;
		}

		/// <summary>
		/// Returns true if the worksheet contains a row with the same string values as the datarow. Must be careful of how 
		/// Excel reformats data (ex. the string 07/07/2007 is entered into Excel as 7/7/2007 by default)
		/// </summary>
		public bool ContainsRow(DataRow dr)
		{
			Excel.Range rngRange;

			object[] arrData = dr.ItemArray;

			bool IsIdentical = true;
			for(int i=1; i <= wsSheet.UsedRange.Rows.Count; i++)
			{
				System.Windows.Forms.Application.DoEvents();

				for(int j=1; j <= arrData.Length; j++)
				{
					rngRange = wsSheet.get_Range(ToAlphaColumn(j) + i.ToString(), objMissing);
					string strCell = rngRange.Text.ToString();
						
					if(strCell != arrData[j-1].ToString())
					{
						IsIdentical = false;
						break;
					}
				}
				
				if(IsIdentical)
					return true;
			}

			return false;
		}

		/// <summary>
		/// Returns true if the worksheet contains a row with the same string value as the datarow in the specified key column
		/// </summary>
		public bool ContainsRow(string strData, string strKeyColumnName)
		{
			int intKeyIndex = GetColumnIndex(strKeyColumnName);
			string strKeyIndex = ToAlphaColumn(intKeyIndex);
			Excel.Range rngRange;

			for(int i=1; i <= wsSheet.UsedRange.Rows.Count; i++)
			{
				System.Windows.Forms.Application.DoEvents();
				rngRange = wsSheet.get_Range(strKeyIndex + i.ToString(), objMissing);
				string strCell = rngRange.Text.ToString();
					
				if(strCell == strData)
					return true;
			}

			return false;
		}

		/// <summary>
		/// Returns true if the worksheet contains a row with the same string values as the datarow in the specified columns
		/// </summary>
		public bool ContainsRow(DataRow dr, string[] arrColumnNames)
		{
			for(int i=1; i <= wsSheet.UsedRange.Rows.Count; i++)
			{
				if(MatchingRow(i, dr, arrColumnNames))
					return true;
			}

			return false;
		}

		/// <summary>
		/// Returns true if the worksheet contains a row with the same string values as the datarow in the specified columns
		/// </summary>
		public virtual bool ContainsRow(DataRow dr, string[] arrColumnNames, out int intRowIndex)
		{
			// Get the indexes for the specified columns in the worksheet
			int[] arrColumnIndexesXL = new int[arrColumnNames.Length];
			for(int i=0; i < arrColumnNames.Length; i++)
			{
				arrColumnIndexesXL[i] = GetColumnIndex(arrColumnNames[i]);
			}

			for(int i=1; i <= wsSheet.UsedRange.Rows.Count; i++)
			{
				if(MatchingRow(i, dr, arrColumnIndexesXL, arrColumnNames))
				{
					intRowIndex = i;
					return true;
				}
			}

			intRowIndex = -1;
			return false;
		}

		protected bool MatchingRow(int intRowIndex, DataRow dr, int[] arrColumnIndexesXL, string[] arrColumnNames)
		{
			Excel.Range rngRange;

			for(int j=0; j < arrColumnNames.Length; j++)
			{
				System.Windows.Forms.Application.DoEvents();
				rngRange = ((Excel.Range)wsSheet.Cells[intRowIndex, arrColumnIndexesXL[j]]);
				string strCell = rngRange.Text.ToString();
					
				if(strCell.ToUpper() != dr[arrColumnNames[j]].ToString().ToUpper())
					return false;
			}

			return true;
		}

		protected bool MatchingRow(int intRowIndex, DataRow dr, string[] arrColumnNames)
		{
			Excel.Range rngRange;

			for(int j=0; j < arrColumnNames.Length; j++)
			{
				System.Windows.Forms.Application.DoEvents();
				int intColIndex = GetColumnIndex(arrColumnNames[j]);
				rngRange = ((Excel.Range)wsSheet.Cells[intRowIndex, intColIndex]);
				string strCell = rngRange.Text.ToString();
					
				if(strCell.ToUpper() != dr[arrColumnNames[j]].ToString().ToUpper())
					return false;
			}

			return true;
		}

		public virtual bool SaveFile()
		{
			if(UtilFile.FileIsThere(strFile))
				wbBook.Save(); // Save the workbook
			else
			{
                //save the workbook to file
                // v1.0.5 -- updated to export as xlsx format; "xlOpenXMLWorkbook"
                wbBook.SaveAs(strFile, Excel.XlFileFormat.xlOpenXMLWorkbook, objMissing, 
					objMissing, objMissing, objMissing, Excel.XlSaveAsAccessMode.xlNoChange, 
					objMissing, objMissing, objMissing, objMissing);
			}

			return true;
		}

		/// <summary>
		/// Converts a 1-based column index to Excel's alpha format (A, B, C, ... AA, AB, AC...)
		/// -- The maximum index returned is ZZ
		/// </summary>
		private string ToAlphaColumn(int intColumn)
		{
			string strRet = "";
			int intMaxCols = 702;	// 26 + (26 * 26) = 702
			if(intColumn >= intMaxCols)
				return "ZZ";

			intColumn--;
			int intDigit1 = intColumn / 26;
			int intDigit2 = intColumn % 26;
			char char1 = Convert.ToChar(65 + intDigit1 - 1);
			char char2 = Convert.ToChar(65 + intDigit2);

			if(intDigit1 > 0)
				strRet = char1.ToString();

			strRet += char2.ToString();

			return strRet;
		}
	}
}
