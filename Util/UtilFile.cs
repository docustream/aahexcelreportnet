using System;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;

namespace dnUtil
{
	/// <summary>
	/// 
	/// </summary>
	
	public class UtilFile
	{
		public UtilFile()
		{
			// 
			// TODO: Add constructor logic here
			//
		}

		public static String AppPath()
		{
			return DirNoSlash(System.AppDomain.CurrentDomain.BaseDirectory);
		}

		public static String DocustreamINI()
		{
			String strDir;
			strDir = ContainingDirectory(System.Environment.SystemDirectory);
			strDir = DirWithSlash(strDir);
			strDir = strDir + "Docustream.ini";
			return strDir;
		}

		public static bool CompFiles(string strFile1, string strFile2)
		{
			if (!System.IO.File.Exists(strFile1) || !System.IO.File.Exists(strFile2)) return false;

			clsFile cFile1 = new clsFile(strFile1, true);
			clsFile cFile2 = new clsFile(strFile2, true);

			if (cFile1.arrLine.Count != cFile2.arrLine.Count)
			{
				return false;
			}
			else if (cFile1.arrLine.Count == 0)
			{
				return false;
			}

			for (int i = 0; i < cFile1.arrLine.Count; i++)
			{
				if ((string)cFile1.arrLine[i] != (string)cFile2.arrLine[i])
				{
					return false;
				}
			}

			return true;
		}

		public static String ContainingDirectory(String strFile)
		{
			String strPath;
			int intSlash;

			strPath = strFile;
			if(strFile=="" || strFile =="\\")
			{
				return strFile;
			}
			intSlash = strFile.LastIndexOf("\\");

			if(strFile.IndexOf(".")<0)
			{
				//it's a directory
				if(intSlash==strFile.Length)
				{
					strPath = strFile.Substring(0, strFile.Length - 1);
				}
			}

			intSlash = strPath.LastIndexOf("\\");

			if(intSlash==0 || intSlash==-1)
			{
				return strFile;
			}
			strPath = strFile.Substring(0, intSlash);
			return strPath;
		}

		public static String DirWithSlash(String strDir)
		{
			String strTemp;
			if(strDir=="")
			{
				return "";
			}
			strTemp = strDir.Substring(strDir.Length - 1, 1);
			if(strTemp!= "\\")
			{
				return strDir + "\\";
			}
			else
			{
				return strDir;
			}
		}

		public static String DirNoSlash(String strDir)
		{
			if (strDir.Length == 0)
				return "";

			if((strDir.Substring(strDir.Length - 1, 1)!= "\\"))
			{
				return strDir;
			}

			if(strDir.Length == 1)
			{
				return "";
			}

			return strDir.Substring(0, strDir.Length -1);
		}

		public static long FileLen(String strFile)
		{
			if (!FileIsThere(strFile))
				return -1;

			FileInfo fi = new FileInfo(strFile);
			return fi.Length;
		}

		public static void FindReplace(string strTextFile, string strFind, string strReplace)
		{
			string strTemp;
			strFind = Regex.Escape(strFind);
			try
			{
				clsFile clsText = new clsFile(strTextFile, true);

				UtilFile.KillFile(clsText.strFile);
				foreach (string str in clsText.arrLine)
				{
					strTemp = Regex.Replace(str, strFind, strReplace, RegexOptions.IgnoreCase);
					UtilFile.WriteToFile(strTemp, clsText.strFile);
				}
			}
			catch
			{

			}
		}

		public static long SplitFile(String strFilePath, ArrayList arrFile)
		{
			arrFile.Clear();

			if(!FileIsThere(strFilePath))
			{
				return -1;
			}
			System.IO.StreamReader sr = new System.IO.StreamReader(strFilePath);

			String strTemp;
			strTemp = sr.ReadLine();

			while(strTemp != null)
			{
				//if(strTemp!="")
				//changed 100906 - we want to preserve blank line info(preserve crlfs)
				arrFile.Add(strTemp);

				strTemp = sr.ReadLine();
			}
			sr.Close();
			sr = null;
			return arrFile.Count;
		}

		public static void WriteToFile(String strText, String strFile)
		{
			try
			{
				StreamWriter sr = new StreamWriter(strFile, true);
				sr.WriteLine (strText);
				sr.Close();
				sr = null;
			}
			catch
			{
				
			}
		}

		public static bool FileIsThere(String strFile)
		{
			return File.Exists(strFile);
		}
		
		public static String FileFromDir(String strFile)
		{
			String strPath;
			int intSlash;

			if(!UtilString.LikeStr(strFile, "*\\*"))
			{
				return strFile;
			}

			if(strFile == "")
			{
				return "";
			}

			intSlash = UtilString.InStrRev(strFile, "\\");
			if(intSlash == 0)
			{
				return "";
			}

			strPath = UtilString.Mid(strFile, intSlash + 1);

			return strPath;
		}

		public static String NameMinusExtension(String strFile)
		{
			int intPos;
			String retVal;
			retVal = "";
			intPos = UtilString.InStrRev(strFile, ".");
			if(intPos <= 1)
			{
				return retVal;
			}

			retVal = UtilString.Mid(strFile, 1, intPos - 1);
			return retVal;
		}

		public static bool DeleteDir(String strdirectory)
		{
			Directory.Delete(strdirectory, true);
			return !Directory.Exists(strdirectory) ? true : false;
		}
		
		public static void KillFile(String strFile)
		{
			try
			{
				File.Delete(strFile);
			}
			catch
			{
				
			}
		}

		public static void KillFileConfirm(String strFile)
		{
			if(File.Exists(strFile))
			{
				System.Windows.Forms.DialogResult dlgResult = System.Windows.Forms.DialogResult.OK;
				do
				{
					try
					{
						File.Delete(strFile);
						dlgResult = System.Windows.Forms.DialogResult.OK;
					}
					catch (Exception ex)
					{
						dlgResult = System.Windows.Forms.MessageBox.Show("Error: " + ex.Message 
							+ "\n\nClose any programs that may be viewing the file: " + strFile,
							"Cannot write to file", System.Windows.Forms.MessageBoxButtons.RetryCancel, 
							System.Windows.Forms.MessageBoxIcon.Warning);
					}
				} while(dlgResult == System.Windows.Forms.DialogResult.Retry);
			}
		}
		
		public static bool DirIsThere(String strdirectory)
		{
			return Directory.Exists(strdirectory);
		}
		
		public static bool MakePath(String strdirectory)
		{
			if(!DirIsThere(strdirectory))
			{
				Directory.CreateDirectory(strdirectory);
				return DirIsThere(strdirectory) ? true : false;
			}
			return true;
		}

		public static bool IsDirectory(String strFile)
		{
			if(UtilFile.FileIsThere(strFile) || UtilFile.DirIsThere(strFile))
			{
				return ((File.GetAttributes(strFile) & FileAttributes.Directory) == FileAttributes.Directory);
			}
			else
			{
				return false;
			}
		}

		public static bool OpenFileCopy(String strFileFrom, String strFileTo)
		{
			return OpenFileCopy(strFileFrom, strFileTo, true);
		}
		
		public static bool OpenFileCopy(String strFileFrom, String strFileTo, bool boolOverWrite)
		{
			if (UtilFile.IsDirectory(strFileTo))
			{
				strFileTo = UtilFile.DirWithSlash(strFileTo) + UtilFile.FileFromDir(strFileFrom);
			}

			try
			{
				File.Copy(strFileFrom, strFileTo, boolOverWrite);
				return true;
			}
			catch
			{
				return false;
			}
		}

	}
}
