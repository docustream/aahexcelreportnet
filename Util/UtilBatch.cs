using System;
using System.Data;
using System.Data.OleDb;
using System.Collections;

namespace dnUtil
{
	/// <summary>
	/// Summary description for UtilBatch.
	/// </summary>
	public class UtilBatch
	{
		public UtilBatch()
		{
			//
			// TODO: Add constructor logic here
			//
		}

        public static ArrayList DocumentImagePathList(OleDbConnection cnBatch)
        {
            ArrayList arrRet = new ArrayList();
            string strFolder = UtilBatch.FolderPath(cnBatch, 1);
            strFolder = UtilFile.DirWithSlash(strFolder);

            OleDbDataReader rd = UtilDB.ExecuteReader(cnBatch, "select * from document order by image_id;");
            while (rd.Read())
            {
                arrRet.Add(strFolder + (string)rd["ImageName"]);
            }
            rd.Close();

            return arrRet;
        }

		public static bool DocumentIsAttachment(OleDbConnection cnBatch, int intImage)
		{
			OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Document] WHERE [Document].[Image_ID] = " + intImage.ToString() + ";", cnBatch);
			OleDbDataReader dr = cmd.ExecuteReader();
			bool boolRet = false;
			
			if(dr.HasRows)
			{
				dr.Read();
				boolRet = Convert.ToBoolean(dr["IsAttachment"]);
			}
			
			dr.Close();
			cmd.Dispose();
			
			return boolRet;
		}
		
		public static String DocumentPathAndImageName(OleDbConnection cnBatch,  int intImage)
		{
			return DocumentPathAndImageName(cnBatch, intImage, false);
		}

		public static String DocumentPathAndImageName(OleDbConnection cnBatch, String strTifNoExt)
		{
			String strRet = "";
			int intImage = 0;
			OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Document] WHERE [Document].[ImageName] = '" + strTifNoExt + ".tif';", cnBatch);
			OleDbDataReader dr = cmd.ExecuteReader();
			
			if(dr.HasRows)
			{
				dr.Read();
				intImage = Convert.ToInt32(dr["Image_ID"]);
			}
			
			dr.Close();
			cmd.Dispose();
			
			strRet = DocumentPathAndImageName(cnBatch, intImage);

			return strRet;
		}
		
		public static String DocumentPathAndImageName(OleDbConnection cnBatch,  int intImage, bool boolFixed)
		{
			String strRet = "", strTemp = "";
			String strImage;
			String strFolder;
			strFolder = UtilBatch.FolderPath(cnBatch, intImage);
			strImage = UtilBatch.DocumentTifName(cnBatch, intImage);
			
			if(strFolder == "" || strImage == "")
			{
				return "";
			}
			
			strFolder = UtilFile.DirWithSlash(strFolder);
			strRet = strFolder + strImage;
			
			if(boolFixed)
			{
				//if it exists, return the fix image path
				strTemp = strFolder + "FixImages\\" + strImage;
					if(UtilFile.FileIsThere(strTemp))
					{
						strRet = strTemp;
					}
			}
			
			return strRet;
		}

		public static String FolderPath(OleDbConnection cnBatch, int intImage)
		{
			String strRet = "";
			OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Folder] WHERE [Folder].[Folder_ID]=1;", cnBatch);
			OleDbDataReader dr = cmd.ExecuteReader();
			
			if(dr.HasRows)
			{
				dr.Read();
				strRet = dr["FolderName"].ToString();
			}
			
			dr.Close();
			cmd.Dispose();
			return strRet;
		}
		
		public static int FormType(OleDbConnection cnBatch, int intImage)
		{
			String SQL;
			int intRet;
			intRet = 0;

			SQL = "";
			SQL = SQL + " SELECT [OCR].[Image_ID], [OCR].[FormType] FROM [OCR]";
			SQL = SQL + " WHERE ((([OCR].[Image_ID])=" + intImage + "));";

			OleDbCommand cmd = new OleDbCommand(SQL, cnBatch);
			OleDbDataReader dr = cmd.ExecuteReader();
			
			if(dr.HasRows)
			{
				dr.Read();
				intRet = Convert.ToInt32(dr["FormType"]);
			}
			dr.Close();
			cmd.Dispose();
			return intRet;
		}
		
		public static String FormName(OleDbConnection cnBatch, int intImage)
		{
			//'returns a given image's formname (string)
			String SQL;
			int intFormType;
			string strRet = "";

			intFormType = FormType(cnBatch, intImage);
			if(intFormType <= 0)
			{
				return "";
			}

			SQL = "";
			SQL = SQL + " SELECT [TRC_MAIN].[TRC_NAME], [TRC_MAIN].[Form_Num]";
			SQL = SQL + " FROM [TRC_MAIN]";
			SQL = SQL + " WHERE ((([TRC_MAIN].[Form_Num])=" + intFormType + "));";

			OleDbCommand cmd = new OleDbCommand(SQL, cnBatch);
			OleDbDataReader dr = cmd.ExecuteReader();
			
			if(dr.HasRows)
			{
				dr.Read();
				strRet = dr["TRC_NAME"].ToString();
			}
			dr.Close();
			cmd.Dispose();
			return strRet;
		}
		
		public static String DocumentTifName(OleDbConnection cnBatch, int intImage)
		{
			String strRet = "";
			OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Document] WHERE [Document].[Image_ID]=" + intImage + ";", cnBatch);
			OleDbDataReader dr = cmd.ExecuteReader();
			
			if(dr.HasRows)
			{
				dr.Read();
				strRet = dr["ImageName"].ToString();
			}
			
			dr.Close();
			cmd.Dispose();
			return strRet;
		}
		
		
		public static int DocumentMaxImage(OleDbConnection cnBatch)
		{
			int intRet=-1;
			OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Document] ORDER BY [Document].[Image_ID] DESC;", cnBatch);
			OleDbDataReader dRead = cmd.ExecuteReader();
			
			if(dRead.HasRows)
			{
				dRead.Read();
				intRet = Convert.ToInt32(dRead["Image_ID"]);
			}
			dRead.Close();
			cmd.Dispose();
			
			return intRet;
		}
		
		public static int EobFieldID(OleDbConnection cnBatch, int intImage, string strCaption)
		{
			string SQL = "select extra_eob_data.id from extra_eob_data inner join client_details on extra_eob_data.id=client_details.id where extra_eob_data.image_id=" + intImage + " and client_details.caption='" + strCaption + "';";
			int intRet = -1;
			OleDbDataReader rd = UtilDB.ExecuteReader(cnBatch, SQL);
			if(rd.HasRows)
			{
				rd.Read();
				intRet = Convert.ToInt32(rd["ID"]);
			}
			rd.Close();
			return intRet;
		}
		
		public static string EOBDataValue(OleDbConnection cnBatch, int intImage, int intID)
		{
			DataTable dTbl = UtilDB.ExecuteQueryDataTable(cnBatch, "select * from extra_eob_data where image_id=" + intImage + " and [id]=" + intID + ";");
			if(dTbl.Rows.Count == 0)
			{
				return "";
			}
			else
			{
				return (string)dTbl.Rows[0]["Data"];
			}
		}
		
		public static int EOBClientMasterID(OleDbConnection cnBatch, string strClientName)
		{
			string SQL = "select * from client_master WHERE Client_name='" + strClientName + "';";
			int intRet = -1;
			OleDbDataReader rd = UtilDB.ExecuteReader(cnBatch, SQL);
			if(rd.HasRows)
			{
				rd.Read();
				intRet = Convert.ToInt32(rd["Client_ID"]);
			}
			rd.Close();
			return intRet;
		}
		
		public static bool FolderChangePath(OleDbConnection cnBatch, String strFolder)
		{
			OleDbDataAdapter da = new OleDbDataAdapter("SELECT * FROM [Folder] WHERE [Folder].[Folder_ID]=1;", cnBatch);
			OleDbCommandBuilder cb = new OleDbCommandBuilder(da);
			DataTable dTbl = new DataTable();
			da.Fill(dTbl);
			
			if(dTbl.Rows.Count == 0)
			{
				DataRow dr = dTbl.NewRow();
				dr["Folder_ID"] = 1;
				dr["FolderName"] =  UtilFile.DirNoSlash(strFolder);
				dTbl.Rows.Add(dr);
			}
			else
			{
				dTbl.Rows[0]["FolderName"] = UtilFile.DirNoSlash(strFolder);
			}
			da.Update(dTbl);

			cb.Dispose();
			da.Dispose();
			return true;
		}

		public static int TRCMinTRC_ID(OleDbConnection cnConn)
		{
			int intRet=-1;
			OleDbCommand cmd = new OleDbCommand("SELECT * FROM [TRC_MAIN] ORDER BY [TRC_MAIN].[TRC_ID]", cnConn);
			OleDbDataReader dRead = cmd.ExecuteReader();
			
			if(!dRead.HasRows)
			{
				goto EndFunction;
			}
			
			dRead.Read();
			intRet = Convert.ToInt32(dRead["TRC_ID"]);
			
			EndFunction:
				dRead.Close();
			cmd.Dispose();
			return intRet;
		}
				
		public static int TRCMaxTRC_ID(OleDbConnection cnConn)
		{
			int intRet=-1;
			OleDbCommand cmd = new OleDbCommand("SELECT * FROM [TRC_MAIN] ORDER BY [TRC_MAIN].[TRC_ID] DESC", cnConn);
			OleDbDataReader dRead = cmd.ExecuteReader();
			
			if(!dRead.HasRows)
			{
				goto EndFunction;
			}
			
			dRead.Read();
			intRet = Convert.ToInt32(dRead["TRC_ID"]);
			
		EndFunction:
			dRead.Close();
			cmd.Dispose();
			return intRet;
		}

		public static int TRCMaxFormNum(OleDbConnection cnConn)
		{
			int intRet=-1;
			OleDbCommand cmd = new OleDbCommand("SELECT * FROM [TRC_MAIN] ORDER BY [TRC_MAIN].[Form_Num] DESC", cnConn);
			OleDbDataReader dRead = cmd.ExecuteReader();
			
			if(!dRead.HasRows)
			{
				goto EndFunction;
			}
			
			dRead.Read();
			intRet = Convert.ToInt32(dRead["Form_Num"]);
			
		EndFunction:
			dRead.Close();
			cmd.Dispose();
			return intRet;
		}

		public static int TRC_FormNum(OleDbConnection cnBatch, String strTRC)
		{
			int intRet=-1;
			OleDbCommand cmd = new OleDbCommand("SELECT * FROM [TRC_MAIN] WHERE [TRC_MAIN].[TRC_NAME]='" + strTRC + "';", cnBatch);
			OleDbDataReader dRead = cmd.ExecuteReader();
			
			if(!dRead.HasRows)
			{
				goto EndFunction;
			}
			
			dRead.Read();
			intRet = Convert.ToInt32(dRead["Form_Num"]);
			
		EndFunction:
			dRead.Close();
			cmd.Dispose();
			return intRet;
		}

		public static int TRC_ID(OleDbConnection cnBatch, String strTRC)
		{
			int intRet=-1;
			OleDbCommand cmd = new OleDbCommand("SELECT * FROM [TRC_MAIN] WHERE [TRC_MAIN].[TRC_NAME]='" + strTRC + "';", cnBatch);
			OleDbDataReader dRead = cmd.ExecuteReader();
			
			if(!dRead.HasRows)
			{
				goto EndFunction;
			}
			
			dRead.Read();
			intRet = Convert.ToInt32(dRead["TRC_ID"]);
			
		EndFunction:
			dRead.Close();
			cmd.Dispose();
			return intRet;
		}
	
		public static String DocumentAddImages(OleDbConnection cnBatch, ArrayList arrTif,
			String strImageFolder)
		{
			//add images to a blank document table
			//arrTif is a sorted list of JUST IMAGE NAMES .. not the full paths
			
			OleDbDataAdapter da = new OleDbDataAdapter("SELECT * FROM [Document];", cnBatch);
			OleDbCommandBuilder cb = new OleDbCommandBuilder(da);
			DataTable dTbl = new DataTable();
			
			da.Fill(dTbl);

			if(dTbl.Rows.Count > 0)
			{
				dTbl.Dispose();
				cb.Dispose();
				da.Dispose();
				return "ERROR: Images already exist in document table";
			}

			String strFileDate;
			String strTif;
			for(int i=0; i<arrTif.Count; i++)
			{
				strTif = UtilFile.DirWithSlash(strImageFolder) + arrTif[i].ToString();
				if(UtilFile.FileIsThere(strTif))
				{
					strFileDate = System.IO.File.GetLastWriteTime(strTif).ToString();
				}
				else
				{
					strFileDate = System.DateTime.Now.ToString();
				}
				DataRow dr = dTbl.NewRow();
				dr["ImageName"] = arrTif[i].ToString();
				dr["Image_ID"] = i + 1;
				dr["IsAttachment"] = false;
				dr["FileDate"] = strFileDate;
				dr["Document_ID"] = 1;
				dr["Folder_ID"] = 1;
				dr["Page_ID"] = 0;
				dr["iImprinterNo"] = 0;
				dTbl.Rows.Add(dr);
			}

			da.Update(dTbl);

			dTbl.Dispose();
			cb.Dispose();
			da.Dispose();
			return "";
		}

        public static int FormsStartRepeat(OleDbConnection cnBatch, String strFormName)
        {
            //'if the form type isn't specified, we'll just assume there's only 1 type of form
            //' in the batch with that zone name.. this is dangerous though
            int intRet = -1;

            DataTable dTbl = UtilDB.ExecuteQueryDataTable(cnBatch,
            "select * from [forms] where [formname]='" + strFormName + "' and [boolstartrepeat]=true;");

            if (dTbl.Rows.Count > 0)
                intRet = Convert.ToInt32(dTbl.Rows[0]["zoneID"]);
            return intRet;
        }

        public static int FormsEndRepeat(OleDbConnection cnBatch, String strFormName)
        {
            //'if the form type isn't specified, we'll just assume there's only 1 type of form
            //' in the batch with that zone name.. this is dangerous though
            int intRet = -1;

            DataTable dTbl = UtilDB.ExecuteQueryDataTable(cnBatch,
                "select * from [forms] where [formname]='" + strFormName + "' and [boolendrepeat]=true;");

            if (dTbl.Rows.Count > 0)
                intRet = Convert.ToInt32(dTbl.Rows[0]["zoneID"]);
            return intRet;
        }

        public static bool FormsZoneIsRepeating(OleDbConnection cnBatch, String strZoneName, String strFormName)
        {
            int intZoneID = FormsZoneID(cnBatch, strZoneName);

            int intStart = FormsStartRepeat(cnBatch, strFormName);
            int intEnd = FormsEndRepeat(cnBatch, strFormName);

            if (intZoneID >= intStart && intZoneID <= intEnd && intZoneID > 0) return true;
            return false;
        }

		public static int FormsZoneID(OleDbConnection cnBatch, String strZone)
		{
			return FormsZoneID(cnBatch, strZone, -1);
		}
		public static int FormsZoneID(OleDbConnection cnBatch, String strZone, int intFormType)
		{
			//'if the form type isn't specified, we'll just assume there's only 1 type of form
			//' in the batch with that zone name.. this is dangerous though
			string SQL;
			int intRet = -1;

			if(intFormType != -1)
			{
				SQL = UtilDB.GetSQL("Forms", "[Forms].[ZoneDescription]='" + strZone + "' AND [Forms].[FormType]=" + intFormType);
			}
			else
			{
				SQL = UtilDB.GetSQL("Forms", "[Forms].[ZoneDescription]='" + strZone + "'");
			}
			OleDbCommand cmd = new OleDbCommand(SQL, cnBatch);
			OleDbDataReader rd = cmd.ExecuteReader();
			
			if(rd.HasRows)
			{
				rd.Read();
				intRet = Convert.ToInt32(rd["ZoneID"]);
			}

			rd.Close();
			cmd.Dispose();
			return intRet;
		}

        public static void OCRInsertEntry(OleDbConnection cnBatch, int intImage, int intFormNum)
        {
            UtilDB.ExecuteNonQuery(cnBatch, "insert into [ocr] (Image_id, FormType) values (" + intImage + ", " + intFormNum + ");");
        }

		public static int ParsedDataDetailsID(OleDbConnection cnBatch, String strDataType)
		{
			int intRet = -1;
			string SQL = UtilDB.GetSQL("ParsedDataDetails", "[ParsedDataDetails].[Description]='" + strDataType + "'");
			OleDbCommand cmd = new OleDbCommand(SQL, cnBatch);
			OleDbDataReader rd = cmd.ExecuteReader();
    
			if(rd.HasRows)
			{
				rd.Read();
				intRet = Convert.ToInt32(rd["ZoneDataType"]);
			}
			rd.Close();
			cmd.Dispose();
			return intRet;
		}

		public static String ParsedDataValue(OleDbConnection cnBatch, int intImage, String strZone, String strDataType)
		{
			return ParsedDataValue(cnBatch, intImage, strZone, strDataType, -1, -1);
		}
		public static String ParsedDataValue(OleDbConnection cnBatch, int intImage, String strZone, String strDataType, 
			int intZoneID)
		{
			return ParsedDataValue(cnBatch, intImage, strZone, strDataType, intZoneID, -1);
		}
		public static String ParsedDataValue(OleDbConnection cnBatch, int intImage, String strZone, String strDataType, 
			int intZoneID, int intDataType)
		{
			//'return single value from parseddata table (ie the city... or the state.. not the whole thing as in the below function)
			//' intDataType will speed up process if you already konw the id
			int intZone;
    
			if(intZoneID < 0)
			{
				intZone = UtilBatch.FormsZoneID(cnBatch, strZone);
			}
			else
			{
				intZone = intZoneID;
			}

			if(intZone <= 0)
			{
				return "";
			}
    
			if(intDataType < 0)
			{
				intDataType = ParsedDataDetailsID(cnBatch, strDataType);
			}
    
			if(intDataType <= 0)
			{
				return "";
			}
			string strRet = "";
			string SQL = UtilDB.GetSQL("ParsedData", "[ParsedData].[ZoneDataType]=" + intDataType + " AND [ParsedData].[ZoneID]=" + intZone + " AND [ParsedData].[Image_ID]=" + intImage);
			OleDbCommand cmd = new OleDbCommand(SQL	, cnBatch);
			OleDbDataReader rd = cmd.ExecuteReader();
            
			if(rd.HasRows)
			{
				rd.Read();
				if(rd["ZoneValue"] != System.DBNull.Value)
				{
					strRet = rd["ZoneValue"].ToString();
				}
			}

			rd.Close();
			cmd.Dispose();
			return strRet;
		}

		public static bool UserDefinedUpdateFromWorkflow(OleDbConnection cnWorkflow, 
			OleDbConnection cnBatch)
		{
			int intTRC;
			bool boolRet = false;
			//now copy over the forms table data
			OleDbDataAdapter da = new OleDbDataAdapter("SELECT * FROM [UserDefined_Fields];", cnWorkflow);
			OleDbCommandBuilder cb = new OleDbCommandBuilder(da);
			DataTable dTbl = new DataTable();

			da.Fill(dTbl);
			cb.Dispose();
			da.Dispose();

			//just get ANY valid TRC for the batch
			intTRC = TRCMinTRC_ID(cnBatch);

			da = new OleDbDataAdapter("SELECT * FROM [UserDefined_Fields];", cnBatch);
			cb = new OleDbCommandBuilder(da);
			DataTable dTblBatch = new DataTable();

			DataRow drNew;
			foreach(DataRow drRow in dTbl.Rows)
			{
				drNew = dTblBatch.NewRow();
				foreach(DataColumn dc in dTbl.Columns)
				{
					if(dc.ColumnName.ToUpper() != "TRC_ID")
					{
						drNew[dc.ColumnName] = drRow[dc.ColumnName];
					}
					else
					{
						drNew[dc.ColumnName] = intTRC;
					}
				}
				dTblBatch.Rows.Add(drNew);
			}
			da.Update(dTblBatch);
			
			boolRet = true;
			dTbl.Clear();
			cb.Dispose();
			da.Dispose();
			return boolRet;
		}
		
		public static String ZoneNameVal(OleDbConnection cnBatch, int intImage, String strZone)
		{
			return ZoneNameVal(cnBatch, intImage, strZone, 1, false);
		}

		public static String ZoneNameVal(OleDbConnection cnBatch, int intImage, String strZone, int intRepeats)
		{
			return ZoneNameVal(cnBatch, intImage, strZone, intRepeats, false);
		}

		public static String ZoneNameVal(OleDbConnection cnBatch, int intImage, String strZone, int intRepeats, bool boolLastRepeat)
		{
			OleDbCommand cmd;
			OleDbDataReader rd;
			String SQL;
			int intForm;
			int intZone;
			int intX;
			string strRet = "";

			intForm = FormType(cnBatch, intImage);
			
			intZone = FormsZoneID(cnBatch, strZone, intForm);
			SQL = "";
			SQL = SQL + " SELECT [Zone].[Image_ID], [Zone].[ZoneID], [Zone].[ZoneValue], [Zone].[ZoneSubID] FROM [Zone]";
			SQL = SQL + " WHERE ((([Zone].[Image_ID])=" + intImage + ")";
			SQL = SQL + " AND (([Zone].[ZoneID])=" + intZone + ")) ORDER BY [Zone].[ZoneSubID];";

			cmd = new OleDbCommand(SQL, cnBatch);
			rd = cmd.ExecuteReader();
			
			if(!rd.HasRows)
			{
				rd.Close();
				cmd.Dispose();
				return "";
			}

			if(boolLastRepeat)
			{
				while(rd.Read())
				{
					strRet = UtilDB.ValueSafe(rd, "ZoneValue");
				}
				//now strRet holds the value of the last repeat line
				goto ReturnValue;
			}

			rd.Read();
			for(intX = 2; intX <= intRepeats; intX++)
			{
				if(!rd.Read())
				{
					//repeat specified is too big
					strRet = "";
					goto ReturnValue;
				}
			}
			
			//if we got here we're on the right repeat
			strRet = UtilDB.ValueSafe(rd, "ZoneValue");

			ReturnValue :
				rd.Close();
			cmd.Dispose();
			return strRet;
		}

		public static int UserDefinedFieldID(OleDbConnection cnBatch, String strFieldName)
		{
			int intRet=-1;
			OleDbCommand cmd = new OleDbCommand("select [UserDefined_FieldID] from [UserDefined_Fields] WHERE [Field_Name]='" + strFieldName + "';", cnBatch);
			OleDbDataReader dRead = cmd.ExecuteReader();
			
			if(!dRead.HasRows)
			{
				goto EndFunction;
			}
			
			dRead.Read();
			intRet = Convert.ToInt32(dRead["UserDefined_FieldID"]);

			EndFunction:
				dRead.Close();
			cmd.Dispose();
			return intRet;
		}

		public static string UserFieldValue(OleDbConnection cnBatch, int intImage, String strFieldName)
		{
			string strRet = "";
			OleDbCommand cmd = new OleDbCommand("select [Field_Value] from [UserField_Value] inner join [UserDefined_Fields] on [UserField_Value].[UserDefined_FieldID]=[UserDefined_Fields].[UserDefined_FieldID] WHERE [UserDefined_Fields].[Field_Name]='" + strFieldName + "' and [UserField_Value].[Image_ID]=" + intImage + ";", cnBatch);
			OleDbDataReader dRead = cmd.ExecuteReader();
			
			if(!dRead.HasRows)
			{
				goto EndFunction;
			}

			dRead.Read();
			if(dRead["Field_Value"] != DBNull.Value)
			{
				strRet = dRead["Field_Value"].ToString();
			}

			EndFunction:
				dRead.Close();
			cmd.Dispose();
			return strRet;
		}

		
		public static string UserFieldValueUpdateSQL(int intUserDefID, int intImage, int intLine, string strSetFieldValue)
		{
			string SQL;
			SQL = 
				"UPDATE [UserField_Value] SET [Field_Value]='" + strSetFieldValue + "' WHERE " + 
				"[UserDefined_FieldID]=" + intUserDefID + " AND [Image_ID]=" + intImage + " AND [Line_No]=" + intLine + ";";
			
			return SQL;
		}

		public static string UserFieldValueInsertSQL(int intUserDefID, int intImage, int intLine, string strSetFieldValue)
		{
			string SQL;
			SQL = 
				"INSERT INTO [UserField_Value] (Field_Value, UserDefined_FieldID, Image_ID, Line_No) VALUES " + //[Field_Value]='" + strSetFieldValue & "' WHERE " + 
				"('" + strSetFieldValue + "', " + intUserDefID + ", " + intImage + ", " + intLine + ");";
			//"[UserDefined_FieldID]=" + intUserDefID + " AND [Image_ID]=" + intImage + " AND [LineID]=" + intLine + ";";
			return SQL;
		}

		public static string UserFieldValueDeleteSQL(int intUserDefID, int intImage, int intLine)
		{
			string SQL;
			SQL = 
				"DELETE FROM [UserField_Value] WHERE [UserDefined_FieldID]=" + intImage + "AND [Image_ID]=" + intImage + " AND [Line_No]=" + intLine + ";";
			return SQL;
		}
		
		public static String ZoneName(OleDbConnection conbatch, int intImage, int intZone)
		{
			//'returns a given zone's zonename (string) using image number to tell what formtype it is

			String SQL, retVal;
			int intFormType;

			retVal = "";
			intFormType = FormType(conbatch, intImage);
			if(intFormType == 0)
			{
				return "";
			}

			SQL = "";
			SQL = SQL + " SELECT [Forms].[FormType], [Forms].[ZoneID], [Forms].[ZoneDescription] FROM [Forms]";
			SQL = SQL + " WHERE ((([Forms].[FormType])=" + intFormType + ")";
			SQL = SQL + " AND (([Forms].[ZoneID])=" + intZone + "));";

			OleDbCommand cmd = new OleDbCommand(SQL, conbatch);
			OleDbDataReader rd = cmd.ExecuteReader();
			
			if(rd.HasRows)
			{
				rd.Read();
				retVal = rd["ZoneDescription"].ToString();
			}

			rd.Close();
			cmd.Dispose();
			return retVal;
		}
		
		public static ArrayList WFTRCGetArr(OleDbConnection cnWorkflow)
		{
			//return an array of strings looking like this:
			// HCFA|1|description...
			// ZReject|7|description...
			//where the 1st part is the form name, the 2nd is the form type
			ArrayList arrRet = new ArrayList();
			OleDbDataReader rd;
			OleDbCommand cmd = new OleDbCommand("select * from TRC_MAIN;", cnWorkflow);
			
			rd = cmd.ExecuteReader();
			String strTemp = "";
			if(rd.HasRows)
			{
				while(rd.Read())
				{
					strTemp = "";
					strTemp = strTemp + rd["TRC_NAME"].ToString();
					strTemp = strTemp + "|" + rd["Form_Num"].ToString() + "|";
					
					if(rd["TRC_DESCRIPTION"] != DBNull.Value)
					{
						strTemp = strTemp + rd["TRC_DESCRIPTION"].ToString();
					}
					arrRet.Add(strTemp);
				}
			}
			
			return arrRet;
		}
	}
}
