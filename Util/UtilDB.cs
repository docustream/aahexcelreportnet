using System;
using System.Data;
using System.Data.OleDb;
using System.Collections;

namespace dnUtil
{
	/// <summary>
	/// Summary description for UtilDB.
	/// </summary>
	public class UtilDB
	{
		public UtilDB()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		
		/// <summary>
		/// Open a microsoft access database
		/// </summary>
		/// <param name="cn">An uninitialized connection variable</param>
		/// <param name="strDatabase">The file path</param>
		/// <returns></returns>
		public static bool DBOpen(out OleDbConnection cn, String strDatabase)
		{
			try
			{
				cn = new OleDbConnection(GetConnStringAccess(strDatabase));
				cn.Open();
				return true;
			}
			catch
			{
				cn = null;
				return false;
			}
		}
		
		public static string ExecuteNonQuery(OleDbConnection cn, String strSQL)
		{
			String strRet = "";
			OleDbCommand cmd = new OleDbCommand(strSQL, cn);
			try
			{
				cmd.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				strRet = ex.Message;
			}
			cmd.Dispose();
		
			return strRet;
		}

        public static String ValueSafe(DataRow rd, string strField)
        {
            return ValueSafe(rd, strField, "");
        }
        public static String ValueSafe(DataRow rd, string strField, string strDefault)
        {
            if (rd[strField] == System.DBNull.Value)
            {
                return strDefault;
            }
            else
            {
                return rd[strField].ToString();
            }
        }

		public static String ValueSafe(OleDbDataReader rd, string strField)
		{
			return ValueSafe(rd, strField, "");
		}
		public static String ValueSafe(OleDbDataReader rd, string strField, string strDefault)
		{
			if(rd[strField] == System.DBNull.Value)
			{
				return strDefault;
			}
			else
			{
				return rd[strField].ToString();
			}
		}

		public static ArrayList TableNames(OleDbConnection cnDB)
		{
			ArrayList arrRet = new ArrayList();
			DataTable dTbl;
			Object[] arrRestrict = new Object[4]  {null, null, null, "TABLE"};
			dTbl = cnDB.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, arrRestrict);
			
			foreach(DataRow dr in dTbl.Rows)
			{
				arrRet.Add(dr["TABLE_NAME"].ToString());
			}
			return arrRet;
		}

        public static DataTable OledbSchemaTables(OleDbConnection cnDB)
        {
            DataTable dTbl;
            Object[] arrRestrict = new Object[4] { null, null, null, "TABLE" };
            dTbl = cnDB.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, arrRestrict);
            return dTbl;
        }

        public static DataTable OledbSchemaColumns(OleDbConnection cnDB, string strTable)
        {
            DataTable dTbl;
            Object[] arrRestrict = new Object[4] { null, null, strTable, null };
            dTbl = cnDB.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, arrRestrict);
            return dTbl;
        }

        public static string GetOLEDBTypeName(OleDbType type)
        {
            string strType = type.ToString().ToUpper();
            if (strType.IndexOf("CHAR") >= 0 ||
               strType.IndexOf("STR") >= 0)
            {
                return "STRING";
            }
            else if (strType.IndexOf("BOOL") >= 0 ||
                strType.IndexOf("BINARY") >= 0)
            {
                return "BOOL";
            }
            else if (strType.IndexOf("TIME") >= 0)
            {
                return "DATETIME";
            }
            else if (strType.IndexOf("DATE") >= 0)
            {
                return "DATETIME";
            }
            else if (strType.IndexOf("INT") >= 0)
            {
                return "INTEGER";
            }
            else if (strType.IndexOf("DOUBLE") >= 0)
            {
                return "INTEGER";
            }
            else if (strType.IndexOf("NUM") >= 0)
            {
                return "INTEGER";
            }
            else if (strType == "DECIMAL")
            {
                return "DECIMAL";
            }
            else if (strType == "CURRENCY")
            {
                return "DECIMAL";
            }
            else
            {
                return "";
            }
        }

		public static object[,] DataTableToArray2D(DataTable dTbl)
		{
			object[,] arr2D = new object[dTbl.Rows.Count, dTbl.Columns.Count];

			for(int i=0; i < dTbl.Rows.Count; i++)
			{
				for(int j=0; j < dTbl.Columns.Count; j++)
				{
					arr2D[i, j] = dTbl.Rows[i][j];
				}
			}

			return arr2D;
		}

		public static bool DataTableContains(DataTable dTbl, string strToFind, string strColumnName)
		{
			foreach(DataRow dr in dTbl.Rows)
			{
				if(strColumnName != "")
				{
					if(dr[strColumnName].ToString() == strToFind)
						return true;
				}
				else // look in all columns
				{
					foreach(DataColumn col in dTbl.Columns)
					{
						if(dr[col.ColumnName].ToString() == strToFind)
							return true;
					}
				}
			}

			return false;
		}

		public static bool DataTableContains(DataTable dTbl, string strToFind, string strColumnName, out int intRow)
		{
			for(int i=0; i < dTbl.Rows.Count; i++)
			{
				if(strColumnName != "")
				{
					if(dTbl.Rows[i][strColumnName].ToString() == strToFind)
					{
						intRow = i;
						return true;
					}
				}
				else // look in all columns
				{
					foreach(DataColumn col in dTbl.Columns)
					{
						if(dTbl.Rows[i][col.ColumnName].ToString() == strToFind)
						{
							intRow = i;
							return true;
						}
					}
				}
			}

			intRow = -1;
			return false;
		}

		public static ArrayList ColumnNames(OleDbConnection cnDB, String strTable)
		{
			//GetOleDbSchemaTable also has the 'DATA_TYPE' field which has integers
			// representing data types (see the oledbtype enum )
			ArrayList arrRet = new ArrayList();
			DataTable dTbl;
			Object[] arrRestrict = new Object[4]  {null, null, strTable, null};
			dTbl = cnDB.GetOleDbSchemaTable(OleDbSchemaGuid.Columns , arrRestrict);
			
			foreach(DataRow dr in dTbl.Rows)
			{
				arrRet.Add(dr["COLUMN_NAME"].ToString());
			}
			return arrRet;
		}

		public static String GetSQL(String strTable, String strCondition)
		{
			return GetSQL(strTable, strCondition, "", false);
		}
		public static String GetSQL(String strTable, String strCondition, String strOrderByColumn)
		{
			return GetSQL(strTable, strCondition, strOrderByColumn, false);
		}
		public static String GetSQL(String strTable)
		{
			return GetSQL(strTable, "", "", false);
		}
		public static String GetSQL(String strTable, String strCondition, String strOrderByColumn, bool boolDescending)
		{
			String SQL;
			SQL = "SELECT * FROM [" + strTable + "]";

			if(strCondition!="")
			{
				SQL += " WHERE " + strCondition;
			}

			if(strCondition!="")
			{
				if(strOrderByColumn != "")
				{
					SQL += " ORDER BY [" + strTable + "].[" + strOrderByColumn +"]";
					if(boolDescending)
					{
						SQL += " DESC";
					}
				}
			}

			SQL += ";";
			return SQL;
		}
		
		public static bool TableIsThere(OleDbConnection cnConn, String strTable)
		{
			OleDbCommand cmd = null;
			try
			{
				cmd = new OleDbCommand("SELECT * FROM [" + strTable + "];", cnConn);
				OleDbDataReader rd = cmd.ExecuteReader();
				rd.Close();
				cmd.Dispose();
				return true;
			}
			catch
			{
				if(cmd != null)
				{
					cmd.Dispose();
				}
				return false;
			}		
		}
		
		public static bool FieldIsThere(OleDbDataReader rd, String strCol)
		{
			try
			{
				int intColOrd = rd.GetOrdinal(strCol);
				return true;
			}
			catch
			{
				return false;
			}
		}

		public static bool ColumnIsThere(OleDbConnection cnConn, String strTable, String strColumn)
		{
			String SQL;
			SQL = "";
			SQL = SQL + " SELECT [" + strTable + "].[" + strColumn + "]";
			SQL = SQL + " FROM [" + strTable + "];";
			
			try
			{
				OleDbCommand cmd = new OleDbCommand(SQL, cnConn);
				OleDbDataReader rd = cmd.ExecuteReader();
				cmd.Dispose();
				rd.Close();
				return true;
			}
			catch
			{
				return false;
			}
		}

		/// <summary>
		/// Get a connection string for a trusted SQL server connection
		/// </summary>
		/// <param name="strServer"></param>
		/// <param name="strDatabase"></param>
		/// <returns></returns>
		public static String GetConnStringSQLServer(String strServer, String strDatabase)
		{	
			//trusted connection string
			return GetConnStringSQLServer(strServer, strDatabase, "", "");
		}

		public static String GetConnStringSQLServer(String strServer, String strDatabase, String strUser, String strPW)
		{	
			//		Provider=sqloledb;Data Source=ServerName;Initial Catalog=DatabaseName;
			//		User Id=Username;Password=Password;

			//For Trusted Connection security: (Microsoft Windows NT integrated security):
			//Provider=sqloledb;Data Source=ServerName;Initial Catalog=DatabaseName;
			//Integrated Security=SSPI;); 

			//To connect to SQL Server running on a remote computer ( via an IP address):
			//Provider=sqloledb;Network Library=DBMSSOCN;Data Source=90.1.1.1,1433; 
			//  Initial Catalog=DatabaseName;User ID=Username;Password=Password;

			string myConnString;
			//use a trusted connection
			if(strUser == "" && strPW == "")
			{
				myConnString = "Provider=SQLOLEDB;Data Source=" + strServer + ";Initial Catalog=" + strDatabase + ";Integrated Security=SSPI;";
			}
			else
			{
				myConnString = "Provider=SQLOLEDB;Data Source=" + strServer + ";Initial Catalog=" + strDatabase + ";User Id=" + strUser + ";Password=" + strPW + ";";
			}
			return myConnString;
		}

		public static String GetConnStringAccess(String strDatabase)
		{
			return "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + strDatabase + ";";
		}		
		
		/// <summary>
		/// tells us if the specified value exists in the specified column
		/// anywhere in the given table (case insensitive)
		/// </summary>
		/// <param name="tbl"></param>
		/// <param name="strCol"></param>
		/// <param name="strValue"></param>
		/// <returns></returns>
		public static bool ValueExists(DataTable tbl, String strCol, String strValue)
		{
			ArrayList arrRet = new ArrayList();
			foreach(DataRow dr in tbl.Rows)
			{
				if(System.DBNull.Value != dr[strCol])
				{
					if(strValue.ToUpper() == dr[strCol].ToString().ToUpper())
					{
						return true;
					}
				}
			}
			return false;
		}		

		/// <summary>
		/// Gets a list of values which are in table1  but not in table2, strCol must be a valid column in both data tables
		/// this function is case insensitive
		/// </summary>
		public static ArrayList CompareDataTables(DataTable tbl1, DataTable tbl2, String strCol)
		{
			//precondition: 
			//
			ArrayList arrRet = new ArrayList();
			String strTemp;
			foreach(DataRow dr in tbl1.Rows)
			{
				System.Windows.Forms.Application.DoEvents();
				if(System.DBNull.Value != dr[strCol])
				{
					strTemp = dr[strCol].ToString();
					if(strTemp != "")
					{
						if(!ValueExists(tbl2, strCol, strTemp))
						{
							arrRet.Add(strTemp);
						}
					}
				}
			}
			return arrRet;
		}

		public static OleDbDataReader ExecuteReader(OleDbConnection cn, string SQL)
		{
			OleDbCommand cmd = new OleDbCommand(SQL, cn);
			OleDbDataReader rdRet = cmd.ExecuteReader();
			cmd.Dispose();
			return rdRet;
		}
		
		/// <summary>
		/// Use this to execute an insert query, and obtain the latest ID for an autonumber field (SQL server only)
		/// </summary>
		/// <param name="cn"></param>
		/// <param name="strQuery"></param>
		/// <returns></returns>
		public static int ExecuteQueryGetIdent(OleDbConnection cn, String strQuery)
		{
			if(!strQuery.EndsWith(";")) strQuery += ";";
			
			OleDbCommand cmd = new OleDbCommand(strQuery + " SELECT @@Identity;", cn);
			OleDbDataReader rd = cmd.ExecuteReader();
			rd.Read();
			int intTemp = Convert.ToInt32(rd[0]);
			rd.Close();
			return intTemp;
		}
		
		/// <summary>
		/// Get the dataTable object resulting from a select query
		/// </summary>
		public static DataTable ExecuteQueryDataTable(OleDbConnection cn, String strQuery)
		{
			DataTable dTblRet = new DataTable();
			OleDbDataAdapter da = new OleDbDataAdapter(strQuery, cn);
			da.Fill(dTblRet);
			da.Dispose();
			return dTblRet;
		}
	}
}
