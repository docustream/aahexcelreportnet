using System;
using System.Text.RegularExpressions;
using System.Collections;
using Microsoft.Win32;

namespace dnUtil
{
	/// <summary>
	/// Summary description for UtilString.
	/// </summary>
	public class UtilString
	{
		public UtilString()
		{
			// TODO: Add constructor logic here
		}

		public static String CharListPunct()
		{
			return ".,;'\"!_-@#$%^&*()+=\\/|?><`~[]{}: \r\n�";
		}

		public static String CharListInt(bool IncludeDec, bool IncludeNeg)
		{
			string strRet= "01232456789";
			if(IncludeDec)
			{
				strRet += ".";
			}
			if(IncludeNeg)
			{
				strRet += "-";
			}
			
			return strRet;
		}

		public static String CharListAlpha()
		{
			return "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		}

		public static int Count(String strSource, String strCount)
		{
			int i;
			int c;
			int lngCount;
			c = 1;
			int retVal;
			if(strCount == "")
			{
				return 0;
			}
    
			lngCount = 0;
			i = UtilString.InStr(c, strSource, strCount);
    
			while(i != 0)
			{
				lngCount = lngCount + 1;
				c = i + strCount.Length;
				i = UtilString.InStr(c, strSource, strCount);
			}	
    
			retVal = lngCount;
			return retVal;
		}

        /// <summary>
        /// return a strNum / 100
        /// </summary>
        public static string DecNumber(string strNum)
        {
            int intTemp = ToNumberDefault(strNum, 0);
            if (intTemp == 0)
            {
                return "0.00";
            }

            Decimal dcTemp = (decimal)intTemp / 100;
            return dcTemp.ToString("#,##0.00");
        }

		public static String DelimField(String str, String strDelim, int intField)
		{
			String strTemp = "";
			int i;
			int intTemp1;

			int intLen = str.Length;
			int intLenDelim = strDelim.Length;

			if(intLen==0 || intLenDelim==0 || intField <=0)
			{
				return "";
			}

			int intCount = Count(str, strDelim);
			if(intCount==0 && intField==1)
			{
				return str;
			}

			if(intField > intCount + 1)
			{
				return "";
			}

			int intCurrPos=1;

			i = 1;
			while(intCurrPos <= intField)
			{
				intTemp1 = InStr(i, str, strDelim);
				if(intTemp1<=0)	
				{
					if(intCurrPos==intField)
					{
						strTemp= Mid(str, i, intLen - i + 1);
					}
					return strTemp;
				}
				else
				{

					strTemp= Mid(str, i, intTemp1 - i);

					if(intCurrPos==intField)
					{	
						return strTemp;
					}
					i = intTemp1 + intLenDelim;
					intCurrPos = intCurrPos + 1;
				}
			}

			return "";
		}

		public static String DelimFieldFromEnd(String str, char strDelim, int intField)
		{
			string []arrTemp = str.Split(strDelim);
			String strRet;
			if(arrTemp.Length > 0)
			{
				if(intField >= 1 && intField <= arrTemp.Length)
				{
					strRet = arrTemp[arrTemp.Length - intField];
				}
				else
				{
					strRet = "";
				}
				return strRet;
			}
			else
			{
				return "";
			}
		}		
		
		public static String FillChars(String strToAppendTo, String strChar, int intTotalLength, bool boolFront)
		{
			while( strToAppendTo.Length < intTotalLength)
			{
				if(boolFront)
				{
					strToAppendTo = strChar + strToAppendTo;
				}
				else
				{
					//'append to back
					strToAppendTo = strToAppendTo + strChar;
				}
			}

			return strToAppendTo;
		}

		public static bool IsDouble(String strCheck)
		{
			double dbl;
			return Double.TryParse(strCheck, System.Globalization.NumberStyles.Float, System.Globalization.NumberFormatInfo.CurrentInfo, out dbl);
		}

		public static bool IsCurrency(String strCheck)
		{
			double dbl;
			return Double.TryParse(strCheck, System.Globalization.NumberStyles.Currency, System.Globalization.CultureInfo.CurrentCulture, out dbl);
		}

		public static bool IsNumber(String strCheck)
		{
			int intTemp;
			try
			{
				intTemp = Convert.ToInt32(strCheck);
				return true;
			}
			catch
			{
				return false;
			}
		}
		
		/// <summary>
		/// Runs 'IsNumber' on each character.  strNum cannot have decimals or negatives
		/// </summary>
		public static bool IsNumberLong(String strNum)
		{
			int i;
			if(strNum == "")
			{
				return true;
			}
			for(i = 0; i<strNum.Length; i++)
			{
				if(!UtilString.IsNumber(strNum.Substring(i, 1)))
				{
					return false;
				}
			}
			return true;
		}
		
		public static int InStrRev(String strValue, String strFind)
		{
			return strValue.LastIndexOf(strFind) + 1;
		}
		
		public static int InStr(int intStart, String strValue, String strFind)
		{
			return strValue.IndexOf(strFind, intStart - 1) + 1;
		}
		
		public static String LikeStrToRegEx(String strLikeStr)
		{
			String strRet;
			strRet = Regex.Escape(strLikeStr); //replaces certain strings with their escape codes
			
			strRet = strRet.Replace("#", "[0-9]");		
			strRet = strRet.Replace("\\#", "[0-9]");
			strRet = strRet.Replace("\\[", "[");
			strRet = "^" + strRet.Replace("\\*", ".*").Replace("\\?", ".") + "$";
			return strRet;
		}

		public static String Left(String str, int intLength)
		{
			if(intLength <= 0)
			{
				return "";
			}
			
			if(str.Length > intLength)
			{
				return str.Substring(0, intLength);
			}
			else
			{
				return str;
			}
		}
		
		public static bool LikeStr(String strToCheck, String strLike)
		{
			//regular expressions
			//isalpha ("[^a-zA-Z]");
			//isalphanumeric ("[^a-zA-Z0-9]");
			//isnumber ("[^0-9]");

			strLike = LikeStrToRegEx(strLike);
			Regex objPositivePattern = new Regex(strLike, RegexOptions.IgnoreCase);
			return objPositivePattern.IsMatch(strToCheck);
		}
		
		public static String Mid(String strValue, int intStart)
		{
			return Mid(strValue, intStart, -1);
		}

		public static String Mid(String strValue, int intStart, int intLen)
		{
			if(intStart < 1)
			{
				intStart = 1;
			}
			if(intStart > strValue.Length)
			{
				return "";
			}
			if(intLen==0)
			{
				return "";
			}
			else if(intStart - 1 + intLen > strValue.Length)
			{
				return strValue.Substring(intStart - 1);
			}
			else if(intLen > 0)
			{
				return strValue.Substring(intStart - 1, intLen);
			}
			else
			{	//intLen < 0.. return the whole rest of the string
				return strValue.Substring(intStart - 1);
			}
		}
		
		/// <summary>
		/// strname must look like Jones, Tom or Jones Tom M
		/// </summary>
		/// <param name="strName"></param>
		/// <returns></returns>
		public static String NameGrabLastName(String strName)
		{
			//'strname must look like this:
			//'jones, tom j
			//'or jones tom m

			int intPos;
			String strRet = "";
			strName = strName.Trim();

			intPos = UtilString.InStr(1, strName, ",");
			if(intPos > 1)
			{
				strRet = UtilString.Mid(strName, 1, intPos - 1).Trim();
			}
			else if(intPos == 0)
			{
				intPos = UtilString.InStr(1, strName, " ");

				if(intPos > 1)
				{
					strRet = UtilString.Mid(strName, 1, intPos - 1).Trim();
				}
				else if(intPos == 0)
				{
					strRet = strName.Trim();
				}
				else if(intPos == 1)
				{
					strRet = "";
				}
			}
			else if(intPos == 1)
			{
				strRet = "";
			}
			return strRet;
		}

		/// <summary>
		/// strname must look like Jones, Tom or Jones Tom M (then it will return 'Tom M')
		/// </summary>
		/// <param name="strName"></param>
		/// <returns></returns>
		public static String NameGrabFirstName(String strName)
		{
			//'strname must look
			//'or jones tom m
			String strRet = "";
			int intPos;
			intPos = InStr(1, strName, ",");
			if(intPos == 0)
			{
				intPos = InStr(1, strName, " ");
				
				if(intPos == 0)
				{
					strRet = "";
				}
				else if(intPos < strName.Length)
				{
					strRet = UtilString.Mid(strName, intPos + 1).Trim();
				}
				else if(intPos == strName.Length)
				{
					strRet = "";
				}
			}
			else if(intPos < strName.Length)
			{
				strRet = UtilString.Mid(strName, intPos + 1).Trim();
			}
			else if(intPos == strName.Length)
			{
				strRet = "";
			}
			return strRet;
		}
		
		/// <summary>
		/// take spaces out of the name before the comma so last names dont get split up 
		/// ie: MC DONNELL, CAROL   or  DE ANGELO, RIVERA
		/// </summary>
		/// <param name="strName"></param>
		/// <returns></returns>
		public static String NameMergeBeforeComma(String strName)
		{
			//'take spaces out of the name before the comma so last names dont get split up
			//' ie: MC DONNELL, CAROL   or  DE ANGELO, RIVERA

			int intFirst;
			int intFirstSpace;
			String retVal;

			intFirst = UtilString.InStr(1, strName, ",");
			intFirstSpace = UtilString.InStr(1, strName, " ");

			//'dont want to merge anything if the comma is the last character
			if((strName.Length != intFirst) && (intFirst > 1))
			{
				if(intFirstSpace < intFirst && intFirstSpace < 4)
				{
					strName = UtilString.Replace(UtilString.Mid(strName, 1, intFirst - 1), " ", "") + UtilString.Mid(strName, intFirst);
				}
			}

			retVal = strName;
			return retVal;
		}
		
		/// <summary>
		/// this function puts bars inbetween the last, first and middle names;
		/// name should be in this format: LAST, FIRST MIDDLE 
		/// or LAST FIRST MIDDLE;
		/// Output looks like this: 'SMITH|JOHN|R.'
		/// </summary>
		/// <param name="strName"></param>
		/// <param name="strDelim"></param>
		/// <returns></returns>
		public static String NameSplit(String strName, String strDelim)
		{
			int firstDelimPos;
			int secondDelimPos;
			int i;
			int length;
			String tempChar;
			String retVal;

			firstDelimPos = -1;
			secondDelimPos = -1;
			length = strName.Length;

			//'this is here because names often come in like ' MC CALLAN, DONNA' and
			//' if we didn't call it, this function would output MC|CALLAN|DONNA
			strName = NameMergeBeforeComma(strName);

			strName = UtilString.Replace(strName, ",", " ");
			strName = ReplaceExtraSpaces(strName);

			for(i = 1; i <= length; i++)
			{
				tempChar = UtilString.Mid(strName, i, 1);
				if(tempChar == " ")
				{
					firstDelimPos = i;
					strName = UtilString.Mid(strName, 1, i - 1) + strDelim + UtilString.Mid(strName, i + 1);
					break;
				}
			}

			//'now reverse from the end
			for(i = length; i >=1; i--)
			{
				tempChar = UtilString.Mid(strName, i, 1);
				if(tempChar == " ")
				{
					secondDelimPos = i;
			        
					if(i <= firstDelimPos)
					{
						break;
					}
			        
					strName = UtilString.Mid(strName, 1, i - 1) + strDelim + UtilString.Mid(strName, i + 1);
			         
					//'if there is another space following, take it out out
					if(UtilString.Mid(strName, i + 1, 1) == " ")
					{
						strName = UtilString.Mid(strName, 1, i) + UtilString.Mid(strName, i + 2);
					}
					break;
				}
			}
			  
			//'there HAS to be 2 delimiters in this string at the end of this function
			while(Count(strName, strDelim) < 2)
			{
				strName = strName + strDelim;
			}

			retVal = strName;
			return retVal;
		}
		
		
		public static String ReplaceExtraSpaces(String str)
		{
			
			while(str.IndexOf("  ", 0) > -1)
			{
				str = str.Replace("  ", " ");
			}
			return str;
		}
		
		public static String ReplacePunct(String str, String strReplaceWith)
		{
			String strPunctList = CharListPunct();
			str = Replace(str, strPunctList, strReplaceWith);
			return str;
		}
		
		public static String ReplaceInt(String str, String strReplaceWith, bool RemoveDec, bool RemoveNeg)
		{
			String strList = CharListInt(RemoveDec, RemoveNeg);
			str = Replace(str, strList, strReplaceWith);
			return str;
		}
		
		public static String ReplaceNonInt(String str, String strReplaceWith, bool RemoveDec, bool RemoveNeg)
		{
			String strList = CharListInt(!RemoveDec, !RemoveNeg);
			str = Replace(str, strList, strReplaceWith, true);
			return str;
		}
		
		public static String ReplaceNonAlpha(String str, String strReplaceWith)
		{
			String strList = CharListAlpha();
			str = Replace(str, strList, strReplaceWith, true);
			return str;
		}

		public static String Replace(String str, String strCharList, String strReplaceWith)
		{
			return Replace(str, strCharList, strReplaceWith, false);
		}

		public static String Replace(String str, String strCharList, String strReplaceWith, bool boolInverse)
		{
			//for just a regular replace function (not a char list, but full strings - use str.Replace)

			//strReplaceChars is a list of characters to replace ie "1234567890" .. would emulate a "replaceint" function
			//                     (unless boolInverse was set to true, then it would emulate a "replacenonint" function
			//strReplaceWith : a string to replace each character in the character list with (this can be any length)
			//boolInverse: if set to false, all characters in the charlist will be replaced, otherwise, everything else
			// will be replaced
			
			int i, c;
			if(str.Length == 0)
			{
				return "";
			}
			
			c = str.Length;
			for(i=c-1; i>=0; i--)
			{
				if(boolInverse)
				{
					if(strCharList.IndexOf(str.Substring(i, 1)) < 0)
					{
						str = str.Remove(i, 1);
						str = str.Insert(i, strReplaceWith);
					}
				}
				else
				{
					if(strCharList.IndexOf(str.Substring(i, 1)) >= 0)
					{
						str = str.Remove(i, 1);
						str = str.Insert(i, strReplaceWith);
					}
				}
			}

			return str;
		}
		
		public static String Right(String str, int intLength)
		{
			if(intLength <= 0)
			{
				return "";
			}
			if(str.Length > intLength)
			{
				return str.Substring(str.Length - intLength);
			}
			else
			{
				return str;
			}
		}
		
		public static void Run(String strCommand, String strArguments)
		{
			//System.Diagnostics.Process prcNew = new System.Diagnostics.Process();
			
			System.Diagnostics.Process.Start(strCommand, strArguments);
			//if(boolWaitFor)
			//{
			//	prcNew.WaitForExit();
			//}
		}
		
		public static ArrayList Split(String str, String split)
		{
			//return an array of strings
			ArrayList arrRet = new ArrayList();
			arrRet.Clear();
	
			String strTemp;
			int i, intLen;
			int intTemp1;
			intLen = str.Length; 
			int intLenSplit;
			intLenSplit = split.Length;

			if(intLen==0 || intLenSplit==0)
			{
				return arrRet;
			}

			int intCount = Count(str, split);
			if(intCount==0)
			{
				arrRet.Add(str);
				return arrRet;
			}

			i = 0;
			while(i <= intLen)
			{
				intTemp1 = str.IndexOf(split, i);
				if(intTemp1<0)
				{
					strTemp = str.Substring(i, intLen - i);
					arrRet.Add(strTemp);
					return arrRet;
				}
				else
				{
					strTemp = str.Substring(i, intTemp1 - i); 
					arrRet.Add(strTemp);
					i = intTemp1 + intLenSplit;
				}
			}
			return arrRet;
		}
		
		public static int StringCompare(String strOne, String strTwo)
		{
			ArrayList arrCompOne = new ArrayList();
			ArrayList arrCompTwo = new ArrayList();
			int countX;
			int countY;
			int countI;
			int countJ;
			int countMatch;
			int lngTotalMatch = 0;
			int countLetter;

			strOne = strOne.ToUpper();
			strTwo = strTwo.ToUpper();
			if (strOne == strTwo)
			{
				return 1000;
			}

			if (strOne.Length == 0 || strTwo.Length == 0)
			{ return 0; }

			for (countLetter = 0; countLetter < strOne.Length; countLetter++)
			{
				arrCompOne.Add(strOne.Substring(countLetter, 1));
			}
			for (countLetter = 0; countLetter < strTwo.Length; countLetter++)
			{
				arrCompTwo.Add(strTwo.Substring(countLetter, 1));
			}

			//'code for fuzzy logic string comparison found at:
			//'http://www.english.upenn.edu/~jlynch/Computing/compare.html
			for (countI = 0; countI < strOne.Length; countI++)
			{
				countX = countI;
				for (countJ = 0; countJ < strTwo.Length; countJ++)
				{
					countY = countJ;
					countMatch = 0;
					while ((countX < strOne.Length) && (countY < strTwo.Length)
						&& (arrCompOne[countX].ToString() == arrCompTwo[countY].ToString()))
					{
						countMatch++;
						lngTotalMatch += countMatch * countMatch;
						countX++;
						countY++;
					}
				}
			}

			lngTotalMatch = (lngTotalMatch * 25) / (strOne.Length * strTwo.Length);

			if (lngTotalMatch > 1000)
			{
				lngTotalMatch = 1000;
			}

			return lngTotalMatch;
		}
		
		/// <summary>
		/// Reformat any string.  strSource, strFormat must be the same length and strFormat must contain
		/// UNIQUE characters.  example: strformat:12345678 strsource:19001122 stroutformat:56/78/1234
		/// Output: 11/22/1900
		/// </summary>
		/// <param name="strSource"></param>
		/// <param name="strFormat"></param>
		/// <param name="strOutFormat"></param>
		/// <returns></returns>
		public static String StringFormat(String strSource, String strFormat, String strOutFormat)
		{
			String strRet = "";
			int intTemp;
			if(strFormat.Length != strSource.Length)
			{
				throw new Exception("String formatting parameters incorrect");
			}

			for(int i=0; i<strOutFormat.Length; i++)
			{
				intTemp = strFormat.IndexOf(strOutFormat[i]);
				if(intTemp < 0)
				{
					strRet += strOutFormat.Substring(i, 1);
				}
				else
				{
					strRet += strSource.Substring(intTemp, 1);
				}
			}
			return strRet;
		}

        public static int ToNumberDefault(string strNumCheck, int intDefault)
        {
            int intRet = intDefault;
            if (IsNumber(strNumCheck))
            {
                intRet = Convert.ToInt32(strNumCheck);
            }
            return intRet;
        }

		public static String TrimLeft(String strValue, String strChar)
		{
			//trim beginning characters
			if(strValue.Length != 0)
			{
				while(strValue.Substring(0, 1) == strChar)
				{
					strValue = strValue.Substring(1);
					if(strValue.Length == 0)
					{
						break;
					}
				}
			}
			return strValue;
		}
	}
}
