using System;
using System.Windows.Forms;

namespace dnUtil
{
	public class Form1 : System.Windows.Forms.Form
	{
		#region Windows Form Designer generated code

		internal System.Windows.Forms.ProgressBar ProgressBar1;
		public System.Windows.Forms.TextBox txtStatus;
		public System.Windows.Forms.Label lblStatus;
		public System.Windows.Forms.Button btnStop;
		public System.Windows.Forms.Button btnStart;
		public System.Windows.Forms.Button btnBrowse;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.Label lblFolder;
		private dnUtil.Controls.TextBoxDroppable txtZipFolder;
		private System.Windows.Forms.Button btnViewReport;
		private dnUtil.Controls.TextBoxDroppable txtOutputFolder;
		private System.Windows.Forms.Label label1;
		public System.Windows.Forms.Button btnBrowse2;
		private dnUtil.Controls.TextBoxDroppable txtReportName;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		public System.Windows.Forms.Button btnExit;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				
			}
			base.Dispose( disposing );
		}

		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ProgressBar1 = new System.Windows.Forms.ProgressBar();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblFolder = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnViewReport = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBrowse2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtReportName = new dnUtil.Controls.TextBoxDroppable();
            this.txtOutputFolder = new dnUtil.Controls.TextBoxDroppable();
            this.txtZipFolder = new dnUtil.Controls.TextBoxDroppable();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ProgressBar1
            // 
            this.ProgressBar1.Location = new System.Drawing.Point(8, 281);
            this.ProgressBar1.Name = "ProgressBar1";
            this.ProgressBar1.Size = new System.Drawing.Size(936, 29);
            this.ProgressBar1.TabIndex = 39;
            // 
            // btnBrowse
            // 
            this.btnBrowse.BackColor = System.Drawing.SystemColors.Control;
            this.btnBrowse.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnBrowse.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowse.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnBrowse.Location = new System.Drawing.Point(936, 22);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnBrowse.Size = new System.Drawing.Size(146, 44);
            this.btnBrowse.TabIndex = 4;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = false;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.SystemColors.Control;
            this.btnStop.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnStop.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnStop.Location = new System.Drawing.Point(952, 214);
            this.btnStop.Name = "btnStop";
            this.btnStop.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnStop.Size = new System.Drawing.Size(146, 44);
            this.btnStop.TabIndex = 1;
            this.btnStop.Text = "S&top";
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.SystemColors.Control;
            this.btnStart.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnStart.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnStart.Location = new System.Drawing.Point(952, 162);
            this.btnStart.Name = "btnStart";
            this.btnStart.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnStart.Size = new System.Drawing.Size(146, 45);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "&Start";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.SystemColors.Control;
            this.btnExit.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnExit.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnExit.Location = new System.Drawing.Point(952, 266);
            this.btnExit.Name = "btnExit";
            this.btnExit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnExit.Size = new System.Drawing.Size(146, 44);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "E&xit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // txtStatus
            // 
            this.txtStatus.AcceptsReturn = true;
            this.txtStatus.BackColor = System.Drawing.SystemColors.Control;
            this.txtStatus.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtStatus.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtStatus.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatus.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtStatus.Location = new System.Drawing.Point(96, 251);
            this.txtStatus.MaxLength = 0;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.ReadOnly = true;
            this.txtStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtStatus.Size = new System.Drawing.Size(840, 30);
            this.txtStatus.TabIndex = 28;
            this.txtStatus.TabStop = false;
            // 
            // lblStatus
            // 
            this.lblStatus.BackColor = System.Drawing.SystemColors.Control;
            this.lblStatus.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblStatus.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblStatus.Location = new System.Drawing.Point(8, 251);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblStatus.Size = new System.Drawing.Size(80, 31);
            this.lblStatus.TabIndex = 38;
            this.lblStatus.Text = "Status:";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblFolder
            // 
            this.lblFolder.Location = new System.Drawing.Point(40, 37);
            this.lblFolder.Name = "lblFolder";
            this.lblFolder.Size = new System.Drawing.Size(120, 29);
            this.lblFolder.TabIndex = 40;
            this.lblFolder.Text = "Zip Folder:";
            // 
            // btnViewReport
            // 
            this.btnViewReport.Location = new System.Drawing.Point(752, 22);
            this.btnViewReport.Name = "btnViewReport";
            this.btnViewReport.Size = new System.Drawing.Size(150, 43);
            this.btnViewReport.TabIndex = 8;
            this.btnViewReport.Text = "&View Report";
            this.btnViewReport.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 29);
            this.label1.TabIndex = 44;
            this.label1.Text = "Report Folder:";
            // 
            // btnBrowse2
            // 
            this.btnBrowse2.BackColor = System.Drawing.SystemColors.Control;
            this.btnBrowse2.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnBrowse2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowse2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnBrowse2.Location = new System.Drawing.Point(936, 74);
            this.btnBrowse2.Name = "btnBrowse2";
            this.btnBrowse2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnBrowse2.Size = new System.Drawing.Size(146, 44);
            this.btnBrowse2.TabIndex = 6;
            this.btnBrowse2.Text = "Browse";
            this.btnBrowse2.UseVisualStyleBackColor = false;
            this.btnBrowse2.Click += new System.EventHandler(this.btnBrowse2_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 29);
            this.label2.TabIndex = 46;
            this.label2.Text = "Report Name:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblFolder);
            this.groupBox1.Controls.Add(this.btnBrowse2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnBrowse);
            this.groupBox1.Controls.Add(this.txtOutputFolder);
            this.groupBox1.Controls.Add(this.txtZipFolder);
            this.groupBox1.Location = new System.Drawing.Point(8, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1096, 133);
            this.groupBox1.TabIndex = 47;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtReportName);
            this.groupBox2.Controls.Add(this.btnViewReport);
            this.groupBox2.Location = new System.Drawing.Point(8, 140);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(920, 82);
            this.groupBox2.TabIndex = 48;
            this.groupBox2.TabStop = false;
            // 
            // txtReportName
            // 
            this.txtReportName.AllowDrop = true;
            this.txtReportName.Location = new System.Drawing.Point(160, 30);
            this.txtReportName.Name = "txtReportName";
            this.txtReportName.Size = new System.Drawing.Size(584, 31);
            this.txtReportName.TabIndex = 7;
            // 
            // txtOutputFolder
            // 
            this.txtOutputFolder.AllowDrop = true;
            this.txtOutputFolder.Location = new System.Drawing.Point(160, 81);
            this.txtOutputFolder.Name = "txtOutputFolder";
            this.txtOutputFolder.Size = new System.Drawing.Size(768, 31);
            this.txtOutputFolder.TabIndex = 5;
            // 
            // txtZipFolder
            // 
            this.txtZipFolder.AllowDrop = true;
            this.txtZipFolder.Location = new System.Drawing.Point(160, 30);
            this.txtZipFolder.Name = "txtZipFolder";
            this.txtZipFolder.Size = new System.Drawing.Size(768, 31);
            this.txtZipFolder.TabIndex = 3;
            // 
            // Form1
            // 
            this.AcceptButton = this.btnStart;
            this.AutoScaleBaseSize = new System.Drawing.Size(10, 24);
            this.ClientSize = new System.Drawing.Size(1106, 321);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.ProgressBar1);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.lblStatus);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "AAHExcelReportNET (0.0.0)";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.Form1_Closing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		public string strProgramName = UtilMisc.GetAssemblyName();
		public string strProgramVersion = UtilMisc.GetAssemblyVersionNoBuild();
		public clsDocuProgram clDocuprogram = null;
		public string strZipFolder = "";
		public string strReportFolder = "";
		public string strReportName = "";
		

		private void Form1_Load(object sender, System.EventArgs e)
		{
			// set form title to assembly name and version
			this.Text = strProgramName + " (" + strProgramVersion + ")";

			UtilMisc.GetSetting(strProgramName, txtZipFolder);
			UtilMisc.GetSetting(strProgramName, txtOutputFolder);
			UtilMisc.GetSetting(strProgramName, txtReportName);

			if(txtOutputFolder.Text == "")
				txtOutputFolder.Text = "TEMP";

			if(txtReportName.Text == "")
			{
                // enter default report name if blank
                // v1.0.5 -- default to xlsx
                txtReportName.Text = "AAH_Report_" + 
					UtilDate.DateFormat(DateTime.Today.ToShortDateString(), "MMDDCCYY") + ".xlsx";
			}
			//else if(UtilString.LikeStr(txtReportName.Text, "*_########.xls"))
			//{
			//	// if date field is at the end, update the date
			//	string strTemp = txtReportName.Text;
			//	strTemp = strTemp.Substring(0, strTemp.LastIndexOf("_") + 1) + 
			//	UtilDate.DateFormat(DateTime.Today.ToShortDateString(), "MMDDCCYY") + ".xls";
			//	txtReportName.Text = strTemp;
			//}

			clDocuprogram = new clsDocuProgram();
			clDocuprogram.Init(strProgramName, this);
		}

		private void btnExit_Click(object sender, System.EventArgs e)
		{
			clDocuprogram = null;
			this.Close();
		}
		
		private void btnStart_Click(object sender, System.EventArgs e)
		{
			// verify folders
			if(!UtilFile.DirIsThere(txtZipFolder.Text))
			{
				MessageBox.Show("Error - Cannot locate folder " + txtZipFolder.Text, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
			if(txtOutputFolder.Text != "" && txtOutputFolder.Text.ToUpper() != "TEMP" && !UtilFile.DirIsThere(txtOutputFolder.Text))
			{
				MessageBox.Show("Error - Cannot locate folder " + txtOutputFolder.Text, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			strZipFolder = txtZipFolder.Text;
			strReportName = txtReportName.Text;

			// if there is no report folder specified (or if TEMP) set to system's temp folder
			if(txtOutputFolder.Text == "" || txtOutputFolder.Text.ToUpper() == "TEMP")
				strReportFolder = System.Environment.GetEnvironmentVariable("TEMP");
			else
				strReportFolder = txtOutputFolder.Text;

			clDocuprogram.Start();
		}

		private void btnStop_Click(object sender, System.EventArgs e)
		{
			clDocuprogram.StopProgram();
		}

		private void Form1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			UtilMisc.SaveSetting(strProgramName, txtZipFolder);
			UtilMisc.SaveSetting(strProgramName, txtOutputFolder);
			UtilMisc.SaveSetting(strProgramName, txtReportName);
		}

		private void btnBrowse_Click(object sender, System.EventArgs e)
		{
			string strTemp = UtilMisc.BrowseFolder(txtZipFolder.Text);
			if(strTemp != "")
				txtZipFolder.Text = strTemp;
		}

		private void btnBrowse2_Click(object sender, System.EventArgs e)
		{
			string strTemp = UtilMisc.BrowseFolder(txtOutputFolder.Text);
			if(strTemp != "")
				txtOutputFolder.Text = strTemp;
		}

		private void btnViewReport_Click(object sender, System.EventArgs e)
		{
			try
			{
				if(clsDocuProgram.strReport != "")
					System.Diagnostics.Process.Start(clsDocuProgram.strReport);
				else
				{
					string strTemp = "";
					if(txtOutputFolder.Text.ToUpper() == "TEMP")
						strTemp = System.Environment.GetEnvironmentVariable("TEMP");
					else
						strTemp = txtOutputFolder.Text;
					System.Diagnostics.Process.Start(UtilFile.DirWithSlash(strTemp) + txtReportName.Text);
				}
			}
			catch 
			{
				MessageBox.Show("Error: Unable to open file " + clsDocuProgram.strReport, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

	}
}
