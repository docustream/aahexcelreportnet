using System;
using System.Data;

namespace dnUtil
{
	/// <summary>
	/// Summary description for clsExcelFileDataTableAAH.
	/// </summary>
	public class clsExcelFileDataTableAAH : clsExcelFileDataTable
	{
		public GUITextFunction GUIMessage = null;
		public GUIProgBarFunction GUIProgBarMessage = null;
		public LogFunction LogMessage = null;

		public clsExcelFileDataTableAAH(string strFileName) : base(strFileName)
		{
		}

		public override bool AddRowsNonIdentical(DataTable dTbl, string[] arrColumnsToCheck, bool blUpdate, string[] arrColumnsToUpdate)
		{
			// only difference between this and the overriden function is that this updates the progress bar

			// we will be adding to the row after the last non-blank row
			int intAddRowIndex = GetLastNonBlankRowDT() + 1;

			int intRowCount = 0;
			GUIProgBar("MAX", dTbl.Rows.Count);

			foreach(DataRow dr in dTbl.Rows)
			{
				GUIProgBar("VALUE", ++intRowCount);
				System.Windows.Forms.Application.DoEvents();

				int intRowIndex = -1;

				if(!ContainsRow(dr, arrColumnsToCheck, out intRowIndex))
				{
					DataRow drNew = dTblExcel.NewRow();
					foreach(DataColumn col in dTbl.Columns)
					{
						drNew[col.ColumnName] = dr[col.ColumnName];
					}
					dTblExcel.Rows.InsertAt(drNew, intAddRowIndex++);
					if(dTblExcel.Rows.Count > intAddRowIndex)
						dTblExcel.Rows.RemoveAt(intAddRowIndex);
				}
				else if(blUpdate)
				{
					if(arrColumnsToUpdate == null)
						UpdateRow(dr, intRowIndex, null);
					else
						UpdateRow(dr, intRowIndex, arrColumnsToUpdate);
				}
			}

			return true;
		}

		protected void Log(string strMessage)
		{
			if(LogMessage != null)
			{
				LogMessage(strMessage);
			}
		}

		protected void GUIText(string strTextBox, string strMessage)
		{
			if(GUIMessage != null)
			{
				GUIMessage(strTextBox, strMessage);
			}
		}
		
		protected int GUIProgBar(string strAction, int intValue)
		{
			if(GUIProgBarMessage != null)
			{
				return GUIProgBarMessage(strAction, intValue);
			}
			return 0;
		}
	}
}
