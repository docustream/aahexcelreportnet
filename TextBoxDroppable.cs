using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace dnUtil
{
	namespace Controls
	{
		/// <summary>
		/// Summary description for TextBoxDroppable.
		/// </summary>
		public class TextBoxDroppable : System.Windows.Forms.TextBox
		{
			private DragDropEffects dragAndDropEffect = DragDropEffects.Copy;
			//private string dataFormat = DataFormats.FileDrop;
			/// <summary>
			/// Required designer variable.
			/// </summary>
			private System.ComponentModel.Container components = null;

			public TextBoxDroppable()
			{
				// This call is required by the Windows.Forms Form Designer.
				InitializeComponent();

				// TODO: Add any initialization after the InitializeComponent call
			}
			
			public TextBoxDroppable(DragDropEffects effect)
			{
				// This call is required by the Windows.Forms Form Designer.
				InitializeComponent();
				// TODO: Add any initialization after the InitializeComponent call
				dragAndDropEffect = effect;
			}
			
//			/// <summary>
//			/// 
//			/// </summary>
//			/// <param name="effect"></param>
//			/// <param name="format">ie DataFormats.FileDrop</param>
//			public TextBoxDroppable(DragDropEffects effect, string format)
//			{
//				// This call is required by the Windows.Forms Form Designer.
//				InitializeComponent();
//				// TODO: Add any initialization after the InitializeComponent call
//				dragAndDropEffect = effect;
//				dataFormat = format;
//			}


			/// <summary> 
			/// Clean up any resources being used.
			/// </summary>
			protected override void Dispose( bool disposing )
			{
				if( disposing )
				{
					if(components != null)
					{
						components.Dispose();
					}
				}
				base.Dispose( disposing );
			}

			#region Component Designer generated code
			/// <summary> 
			/// Required method for Designer support - do not modify 
			/// the contents of this method with the code editor.
			/// </summary>
			private void InitializeComponent()
			{
				// TextBoxDroppable
				this.Name = "TextBoxDroppable";
				this.Size = new System.Drawing.Size(349, 88);
				this.AllowDrop = true;
				this.DragEnter += new System.Windows.Forms.DragEventHandler(this.TextBoxDragEnter);
				this.DragDrop += new  System.Windows.Forms.DragEventHandler(this.TextBoxDragDrop);
			}
			#endregion
			
			private string GetDataFormat(System.Windows.Forms.DragEventArgs e)
			{
				string[] arrTemp = e.Data.GetFormats();
				
				foreach(string strTemp in arrTemp)
				{
					if(strTemp.ToUpper() == "FILEDROP")
					{
						return DataFormats.FileDrop;
					}
					else if(strTemp.ToUpper() == "TEXT")
					{
						return DataFormats.Text;
					}
				}
				
				return "";
			}

			private void TextBoxDragEnter(object sender, System.Windows.Forms.DragEventArgs e)
			{
				//Cursor.Current = cursorOver;
				//e.Effect = effect;
				// make sure they're actually dropping files or text (not anything else.. like a bitmap)
				
				string strTemp = GetDataFormat(e);
				if(strTemp == DataFormats.FileDrop || strTemp == DataFormats.Text)
					e.Effect = dragAndDropEffect;
			}

			private void TextBoxDragDrop(object sender, System.Windows.Forms.DragEventArgs e)
			{
				string dataFormat = GetDataFormat(e);
				
				if(dataFormat == DataFormats.FileDrop)
				{
					
						String[] arrFile = (string[])e.Data.GetData(DataFormats.FileDrop);
						
						e.Data.GetData(DataFormats.Text);
						if(arrFile.Length > 0)
						{
							this.Text = arrFile[0].ToString();
						}
				}
				else if(dataFormat == DataFormats.Text)
				{
						string strTemp = (string)e.Data.GetData(DataFormats.Text);
						this.Text = strTemp;
				}
				
				Cursor.Current = Cursors.Default;
			}
		}
	}
}
